#!/usr/bin/python

from os import listdir
from os.path import isfile, join
from subprocess import call
from os import makedirs
from distutils.dir_util import copy_tree
from os import remove
from shutil import copyfile


promo_path = "promo/default"
input_path = "www/nindex/"
output_path = "public"


for path in ["assets/js/", "images/"]:
	out_p = join(output_path,path)
	copy_tree(join(input_path,path), join(output_path, path))


out_p = join(output_path, "assets/css/")
try:
    makedirs(out_p)
except OSError as exc:  # Python >2.5
    print exc
call(["sass", "-C", join(input_path,"assets/sass/main.scss"), join(output_path, "assets/css/main.css")])
remove(join(output_path, "assets/css/main.css.map"))

file_name_path = "www/paths.txt"
working_file = "index.html"
input_file = join(input_path, working_file)
output_file = join(output_path, working_file)

with open (input_file, "r") as myfile:
	data=myfile.read()
	myfile.close()


promo_files = listdir(promo_path)

for file_name in promo_files:
	file_path = join(promo_path, file_name)
	if isfile(file_path):
		string_to_replace = "${" + file_name + "}"
		with open(file_path, "r") as replace_file:
			data_to_replace = replace_file.read()
			data = data.replace(string_to_replace, data_to_replace)
			replace_file.close()

with open(output_file, "w") as outfile:
	outfile.write(data)
	outfile.close()
	
input_background = join(promo_path, "screenshoots/tasks_list.png")
output_background = join(output_path, "images/background.png")
copyfile(input_background, output_background)

