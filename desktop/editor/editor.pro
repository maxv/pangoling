TEMPLATE = app
TARGET = pangoling-editor

QT += core gui widgets

CONFIG += c++11

INCLUDEPATH += src

DEFINES += BASE_PATH=\\\"$${PWD}/../..\\\"

SOURCES += \
    src/main.cpp \
    src/Course.cpp \
    src/gui/TaskWidget.cpp \
    src/gui/ImagePathDelegate.cpp \
    src/models/TaskModel.cpp \
    src/gui/CourseWidget.cpp \
    src/Utils.cpp \
    src/gui/LintResultDialog.cpp \
    src/Lint.cpp

HEADERS += \
    src/JsonConstants.h \
    src/Course.h \
    src/gui/TaskWidget.h \
    src/gui/ImagePathDelegate.h \
    src/models/TaskModel.h \
    src/gui/CourseWidget.h \
    src/Utils.h \
    src/gui/LintResultDialog.h \
    src/Lint.h
