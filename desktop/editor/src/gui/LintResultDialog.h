#ifndef LINTRESULTDIALOG_H
#define LINTRESULTDIALOG_H

#include <QDialog>
#include <QStringListModel>

#include "Lint.h"

class LintResultDialog : public QDialog
{
	Q_OBJECT
public:
	static void ShowDialog(Lint::Result lintResult, QWidget* parent);
	explicit LintResultDialog(QStringList unusedFiles, QWidget *parent = 0);
private slots:
	void onRemoveAll();
private:
	QStringListModel m_stringListModel;
};

#endif // LINTRESULTDIALOG_H
