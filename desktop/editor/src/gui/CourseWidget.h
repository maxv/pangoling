#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <memory>

#include <QWidget>
#include <QStringListModel>
#include <QJsonParseError>
#include <QModelIndex>
#include <QItemSelection>
#include <QFileInfo>

#include "Course.h"

class QListView;
class QStringListModel;
class QItemSelectionModel;
class TaskWidget;
class QLineEdit;
class QTextEdit;
class QLabel;


class CourseWidget : public QWidget
{
	Q_OBJECT
public:
	explicit CourseWidget(QWidget *parent = 0);
signals:
	void onFileOpeningError();
	void onFileParseError(QJsonParseError error);
	void onJsonInWrongFomat();
	
	void onTaskSelected(Course::Task task);
public slots:
	void onNewRequested();
	void onSaveRequested();
	void onLoadRequested();
	void loadModel(QFileInfo pathToJson);
private slots:
	void onSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
	
	void onSaveTaskRequested(Course::Task task);
	
	void onSetImageRequested();
	void onRemoveRequested(QString answer);
	void onLintRequested();
private:
	QStringListModel* m_model;
	QListView* m_listView;
	
	void updateView();
	std::unique_ptr<Course> m_course;
	
	QItemSelectionModel* m_itemSelectionModel;
	TaskWidget* m_taskWidget;
	
	QLineEdit* m_courseName;
	QTextEdit* m_description;
	QLabel* m_courseImage;
	QString m_courseImagePath;
	QLabel* m_uuid;
	
	void setCourseImage(QString path);
};

#endif // MAINWIDGET_H
