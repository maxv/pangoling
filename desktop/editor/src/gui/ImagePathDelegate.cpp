#include "ImagePathDelegate.h"

#include <QPainter>
#include <QDir>

ImagePathDelegate::ImagePathDelegate(QObject* parent): QStyledItemDelegate(parent)
{
	
}

void ImagePathDelegate::paint(
	QPainter* painter, 
	const QStyleOptionViewItem& option, 
	const QModelIndex& index
) const 
{
	painter->save();
	
	QString path = index.data().toString();
	
	QImage* image = getFromCacheOrLoad(path);
	
	if (image != nullptr)
	{
		QRect imageRect = image->rect();
		imageRect.moveTo(option.rect.left(), option.rect.top());
		QRect rect = option.rect.intersected(imageRect);
		painter->drawImage(rect, m_cache[path]);
	}
	else
	{
		painter->setPen(QPen(QColor(200, 20, 20)));
		painter->drawText(option.rect, path);
	}
	
	painter->restore();
}

QSize ImagePathDelegate::sizeHint(
	const QStyleOptionViewItem& option, 
	const QModelIndex& index
) const
{
	QString path = index.data().toString();
	QImage* image = getFromCacheOrLoad(path);
	if (image != nullptr)
	{
		return image->size();
	}
	else
	{
		return QStyledItemDelegate::sizeHint(option, index);
	}
}

QImage*ImagePathDelegate::getFromCacheOrLoad(const QString& path) const
{
	QImage* result = nullptr;
	
	if (!m_cache.contains(path)) 
	{
		QImage image = QImage(m_basePath + QDir::separator() + path);
		if (!image.isNull())
		{
			m_cache.insert(path, image);
		}
	}
	
	if (m_cache.contains(path))
	{
		result = &m_cache[path];
	}
	
	return result;
}

QString ImagePathDelegate::getBasePath() const
{
	return m_basePath;
}

void ImagePathDelegate::setBasePath(const QString& basePath)
{
	m_basePath = basePath;
	m_cache.clear();
}
