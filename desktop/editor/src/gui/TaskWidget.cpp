#include "TaskWidget.h"

#include <QLineEdit>
#include <QListView>
#include <QVBoxLayout>
#include <QPushButton>
#include <QStringListModel>
#include <QFileDialog>

#include "ImagePathDelegate.h"
#include "Utils.h"

TaskWidget::TaskWidget(QWidget *parent)
	: QWidget(parent)
	, m_answer(new QLineEdit)
	, m_itemsList(new QListView)
	, m_imagesPaths(new QStringListModel(this))
	, m_imagesDelegate(new ImagePathDelegate(this))
{
	QVBoxLayout* mainLayout = new QVBoxLayout();
	this->setLayout(mainLayout);
	
	QPushButton* add = new QPushButton("Add");
	connect(add, &QPushButton::clicked, this, &TaskWidget::addImageRequested);
	
	QPushButton* save = new QPushButton("Save");
	connect(save, &QPushButton::clicked, this, &TaskWidget::onSaveClicked);
	
	QPushButton* new_ = new QPushButton("New");
	connect(new_, &QPushButton::clicked, this, &TaskWidget::onNewClicked);
	
	QPushButton* remove = new QPushButton("Remove");
	connect(remove, &QPushButton::clicked, this, &TaskWidget::onRemoveClicked);
	
	QHBoxLayout* answerAndButtonsLayout = new QHBoxLayout();
	answerAndButtonsLayout->addWidget(save);
	answerAndButtonsLayout->addWidget(add);
	answerAndButtonsLayout->addWidget(new_);
	answerAndButtonsLayout->addWidget(remove);
	
	mainLayout->addLayout(answerAndButtonsLayout);
	mainLayout->addWidget(m_answer);
	mainLayout->addWidget(m_itemsList, 1);
	
	m_itemsList->setModel(m_imagesPaths);
	
	m_itemsList->setItemDelegate(m_imagesDelegate);
}

void TaskWidget::setTask(Course::Task task)
{
	m_answer->setText(task.answer());
	m_imagesPaths->setStringList(task.getImages());
	m_task = task;
}

void TaskWidget::addImageRequested()
{
	QFileDialog fileDialog;
	if (fileDialog.exec())
	{
		QStringList paths = fileDialog.selectedFiles();
		int size = paths.size();
		m_imagesPaths->insertRows(0, size);
		for(int i = 0; i < size; ++i)
		{
			QModelIndex index = m_imagesPaths->index(i);
			QString path = Utils::addFileToCourse(paths[i], m_basePath);
			
			m_imagesPaths->setData(index, path);
		}
	}
}

void TaskWidget::onSaveClicked()
{
	Course::Task task;
	task.setAnswer(m_answer->text());
	
	for(QString& s: m_imagesPaths->stringList())
	{
		task.addImage(s);
	}
	
	emit onSaveRequested(task);
}

void TaskWidget::onNewClicked()
{
	setTask(Course::Task());
}

void TaskWidget::onRemoveClicked()
{
	emit onRemoveRequested(m_task.answer());
}

QString TaskWidget::basePath() const
{
	return m_basePath;
}

void TaskWidget::setBasePath(const QString& basePath)
{
	m_basePath = basePath;
	m_imagesDelegate->setBasePath(basePath);
	m_imagesPaths->setStringList(QStringList());
	m_answer->setText("");
}

