#ifndef TASKWIDGET_H
#define TASKWIDGET_H

#include <QWidget>

#include "Course.h"

class QLineEdit;
class QListView;
class QStringListModel;
class ImagePathDelegate;

class TaskWidget : public QWidget
{
	Q_OBJECT
public:
	explicit TaskWidget(QWidget *parent = 0);
	
	QString basePath() const;
	void setBasePath(const QString& basePath);
	
signals:
	void onSaveRequested(Course::Task task);
	void onRemoveRequested(QString answer);
public slots:
	void setTask(Course::Task task);
private slots:
	void addImageRequested();
	void onSaveClicked();
	void onNewClicked();
	void onRemoveClicked();
private:
	QLineEdit* m_answer;
	QListView* m_itemsList;
	QStringListModel* m_imagesPaths;
	QString m_basePath;
	Course::Task m_task;
	ImagePathDelegate* m_imagesDelegate;
};

#endif // TASKWIDGET_H
