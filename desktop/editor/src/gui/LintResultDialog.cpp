#include "LintResultDialog.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QListView>
#include <QLabel>
#include <QMessageBox>
#include <QFile>

void LintResultDialog::ShowDialog(Lint::Result lintResult, QWidget* parent)
{
	bool allOk = true;
	if (!lintResult.getUnused().isEmpty())
	{
		allOk = false;
		
		LintResultDialog d{lintResult.getUnused(), parent};
		d.exec();
	}
	
	if (!lintResult.getNotFound().isEmpty())
	{
		allOk = false;
		
		QString text = QString("FilesNotFound:\n%1").arg(lintResult.getNotFound().join("\n"));
		QMessageBox unused{parent};
		unused.setIcon(QMessageBox::Warning);
		unused.setText(text);
		unused.addButton("Ok", QMessageBox::AcceptRole);
		unused.exec();
	}
	
	if (allOk)
	{
		QMessageBox ok{parent};
		ok.setIcon(QMessageBox::Information);
		ok.setText("Lint passed without any errors.");
		ok.addButton("Ok", QMessageBox::AcceptRole);
		ok.exec();
	}
}

LintResultDialog::LintResultDialog(QStringList unusedFiles, QWidget *parent) 
	: QDialog(parent)
	, m_stringListModel(unusedFiles, this)
{
	QVBoxLayout* mainLayout = new QVBoxLayout;
	this->setLayout(mainLayout);
	
	QListView* filesList = new QListView;
	filesList->setModel(&m_stringListModel);
	
	QPushButton* remove = new QPushButton("!! Remove");
	connect(remove, &QPushButton::clicked, this, &LintResultDialog::onRemoveAll);
	
	QPushButton* cancel = new QPushButton("Cancel");
	connect(cancel, &QPushButton::clicked, this, &LintResultDialog::close);
	
	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addStretch();
	buttonsLayout->addWidget(remove);
	buttonsLayout->addWidget(cancel);
	
	mainLayout->addWidget(new QLabel("Unused files:"));
	mainLayout->addWidget(filesList);
	mainLayout->addLayout(buttonsLayout);
}

void LintResultDialog::onRemoveAll()
{
	QStringList fileNames = m_stringListModel.stringList();
	QString dialogText = QString("Are you really want to remove:\n%1").arg(fileNames.join("\n"));
	
	QMessageBox dialog{this};
	dialog.setText(dialogText);
	dialog.addButton("Remove all", QMessageBox::AcceptRole);
	dialog.addButton("Cancel", QMessageBox::RejectRole);
	dialog.setIcon(QMessageBox::Critical);
	if ( dialog.exec() == QMessageBox::AcceptRole)
	{
		for(QString fileName: fileNames)
		{
			QFile::remove(fileName);
		}
		
		accept();
	}
}
