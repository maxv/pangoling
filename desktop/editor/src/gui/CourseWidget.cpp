#include "CourseWidget.h"

#include <iostream>

#include <QDir>
#include <QFile>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListView>

#include <QStringListModel>
#include <QItemSelectionModel>

#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>
#include <QTextEdit>
#include <QLabel>
#include <QMessageBox>

#include "Utils.h"
#include "Lint.h"
#include "gui/LintResultDialog.h"
#include "gui/TaskWidget.h"

CourseWidget::CourseWidget(QWidget *parent) : 
	QWidget(parent)
  , m_model(new QStringListModel)
  , m_listView(new QListView)
  , m_itemSelectionModel(new QItemSelectionModel(m_model))
  , m_taskWidget(new TaskWidget)
  , m_courseName(new QLineEdit)
  , m_description(new QTextEdit)
  , m_courseImage(new QLabel)
  , m_uuid(new QLabel)
{
	QHBoxLayout* mainLayout = new QHBoxLayout();
	this->setLayout(mainLayout);
	
	QPushButton* newButton = new QPushButton("New");
	QPushButton* save = new QPushButton("Save");
	QPushButton* load = new QPushButton("Load");
	QPushButton* lint = new QPushButton("Lint...");
	
	connect(save, &QPushButton::clicked, this, &CourseWidget::onSaveRequested);
	connect(newButton, &QPushButton::clicked, this, &CourseWidget::onNewRequested);
	connect(load, &QPushButton::clicked, this, &CourseWidget::onLoadRequested);
	connect(lint, &QPushButton::clicked, this, &CourseWidget::onLintRequested);
	
	QHBoxLayout* buttonsLayout = new QHBoxLayout();
	buttonsLayout->addWidget(newButton);
	buttonsLayout->addWidget(save);
	buttonsLayout->addWidget(load);
	buttonsLayout->addWidget(lint);
	
	QHBoxLayout* courseName = new QHBoxLayout();
	courseName->addWidget(new QLabel("Course name:"));
	courseName->addWidget(m_courseName);
	
	QPushButton* setImage = new QPushButton("Set image...");
	connect(setImage, &QPushButton::clicked, this, &CourseWidget::onSetImageRequested);
	
	QVBoxLayout* courseAbout = new QVBoxLayout();
	courseAbout->addLayout(courseName);
	courseAbout->addWidget(m_uuid);
	courseAbout->addWidget(m_description);
	courseAbout->addWidget(setImage);
	courseAbout->addWidget(m_courseImage);
	m_courseImage->setMinimumSize(200,200);
	
	QVBoxLayout* leftLayout = new QVBoxLayout();
	leftLayout->addLayout(buttonsLayout);
	leftLayout->addLayout(courseAbout);
	
	
	mainLayout->addLayout(leftLayout);
	mainLayout->addWidget(m_listView);
	mainLayout->addWidget(m_taskWidget, 1);
	
	m_listView->setModel(m_model);
	m_listView->setSelectionModel(m_itemSelectionModel);
	
	
	connect(
		m_itemSelectionModel, &QItemSelectionModel::selectionChanged, 
		this, &CourseWidget::onSelectionChanged
	);
	
	connect(
		this, &CourseWidget::onTaskSelected,
		m_taskWidget, &TaskWidget::setTask
	);
	
	connect(
		m_taskWidget, &TaskWidget::onSaveRequested,
		this, &CourseWidget::onSaveTaskRequested
	);
	
	connect(
		m_taskWidget, &TaskWidget::onRemoveRequested,
		this, &CourseWidget::onRemoveRequested
	);
}

void CourseWidget::loadModel(QFileInfo pathToJson)
{
	QFile f{pathToJson.filePath()};
	if (!f.open(QFile::ReadOnly)) 
	{
		emit onFileOpeningError();
	}
	else
	{
		QByteArray data = f.readAll();
		QJsonParseError parserError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(data, &parserError);
		if (parserError.error != QJsonParseError::NoError) 
		{
			emit onFileParseError(parserError);
		}
		else
		{
			if (!jsonDocument.isArray())
			{
				emit onJsonInWrongFomat();
			}
			else
			{
				QJsonArray courses = jsonDocument.array();
				for(QJsonValue courseJson: courses)
				{
					if (!courseJson.isObject())
					{
						emit onJsonInWrongFomat();
						break;
					}
					else
					{
						QJsonObject o = courseJson.toObject();
						m_course = std::unique_ptr<Course>(new Course(o));
						m_taskWidget->setBasePath(pathToJson.dir().path());
						updateView();
					}
				}
			}
		}
	}
}

void CourseWidget::onSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
	int row = selected.first().indexes().first().row();
	Course::Task task = m_course->taskAt(row);
	emit onTaskSelected(task);
}

void CourseWidget::onSaveRequested()
{
	m_course->setName(m_courseName->text());
	m_course->setDescription(m_description->toPlainText());
	m_course->setImageName(m_courseImagePath);
	
	QString fileName = m_taskWidget->basePath() + QDir::separator() + Course::jsonFileName();
	
	QJsonArray array;
	array.push_back(m_course->toJson());
	
	QJsonDocument d;
	d.setArray(array);
	
	QFile f(fileName);
	if (f.open(QFile::WriteOnly))
	{
		f.write(d.toJson());
	}
}

void CourseWidget::onSaveTaskRequested(Course::Task task)
{
	m_course->replaceTask(task);
	updateView();
}

void CourseWidget::onSetImageRequested()
{
	QString path = QFileDialog::getOpenFileName(
		this,
		"Select course image"
	);
	
	if (!path.isEmpty())
	{
		path = Utils::addFileToCourse(path, m_taskWidget->basePath());
		setCourseImage(path);
	}
}

void CourseWidget::onRemoveRequested(QString answer)
{
	m_course->removeTask(answer);
	updateView();
}

void CourseWidget::onLintRequested()
{
	QString basePath = m_taskWidget->basePath();
	if (!basePath.isEmpty()) 
	{
		Lint::Result lintResult = Lint::check(*m_course, m_taskWidget->basePath());
		LintResultDialog::ShowDialog(lintResult, this);
	}
	else
	{
		QMessageBox::information(this, "Lint", "Course not loaded.", QMessageBox::Ok);
	}
	
}

void CourseWidget::onNewRequested()
{
	QString path = QFileDialog::getExistingDirectory(this);
	if (!path.isEmpty())
	{
		m_course = std::unique_ptr<Course>(new Course());
		m_taskWidget->setBasePath(path);
		updateView();
	}
}

void CourseWidget::updateView()
{
	QStringList tasks = m_course->tasksName();
	m_model->setStringList(tasks);
	
	m_courseName->setText(m_course->name());
	m_description->setText(m_course->description());
	m_uuid->setText(QString("UUID:%1").arg(m_course->uuid()));
	
	setCourseImage(m_course->imageName());
}

void CourseWidget::setCourseImage(QString path)
{
	m_courseImagePath = path;
	QImage image = QImage(m_taskWidget->basePath() + QDir::separator() + m_courseImagePath);
	if (image.isNull())
	{
		m_courseImage->setPixmap(QPixmap());
		m_courseImage->setText("No image");
		m_courseImagePath = "";
	}
	else
	{
		m_courseImage->setPixmap(QPixmap::fromImage(image));
		m_courseImage->setText("");
	}
}

void CourseWidget::onLoadRequested()
{
	QString path = QFileDialog::getOpenFileName(this, "Load course", BASE_PATH, Course::jsonFileName());
	if (!path.isEmpty())
	{
		QFileInfo fileInfo{path};
		loadModel(fileInfo);
	}
}
