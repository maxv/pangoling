#ifndef IMAGEPATHDELEGATE_H
#define IMAGEPATHDELEGATE_H

#include <QStyledItemDelegate>
#include <QMap>

class ImagePathDelegate : public QStyledItemDelegate
{
	Q_OBJECT
public:
	ImagePathDelegate(QObject* parent = 0);
	void paint(
		QPainter *painter,
		const QStyleOptionViewItem &option,
		const QModelIndex &index
	) const Q_DECL_OVERRIDE;
	
    QSize sizeHint(
		const QStyleOptionViewItem &option,
		const QModelIndex &index
	) const Q_DECL_OVERRIDE;
	
	
	QString getBasePath() const;
	void setBasePath(const QString& basePath);
	
private:
	QImage* getFromCacheOrLoad(const QString& path) const;
	mutable QMap<QString, QImage> m_cache;
	QString m_basePath;
};

#endif // IMAGEPATHDELEGATE_H
