#include "Lint.h"

#include <Course.h>
#include <QDir>

Lint::Result Lint::check(const Course& course, QString path)
{
	Lint::Result result;
	
	int count = course.tasksCount();
	for(int i = 0; i < count; ++i)
	{
		QStringList images = course.taskAt(i).getImages();
		for(QString imagePath: images)
		{
			result.m_notFound.append(path + imagePath);
		}
	}
	result.m_notFound.append(path + course.imageName());
	result.m_notFound.append(path + QDir::separator() + Course::jsonFileName());
	
	QStringList dirs;
	dirs.append(path);
	while(!dirs.empty())
	{
		QDir d = QDir(dirs.first());
		dirs.pop_front();
		dirs.append(
			d.entryList(
				QDir::NoDotAndDotDot|
				QDir::Dirs
			)
		);
		
		for(QString fileName: d.entryList(QDir::Files))
		{
			QString filePath = d.absoluteFilePath(fileName);
			bool fileExistInCourse = result.m_notFound.contains(filePath);
			if (fileExistInCourse)
			{
				result.m_notFound.removeAll(filePath);
			}
			else
			{
				result.m_unused.append(filePath);
			}
		}
		
	}
	
	return result;
}
