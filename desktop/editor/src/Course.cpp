#include "Course.h"

#include <QJsonArray>
#include <QJsonValue>
#include <QUuid>

const QString Course::KEY_NAME = "name";
const QString Course::KEY_DESCRIPTION = "description";
const QString Course::KEY_IMAGE_NAME = "imageName";
const QString Course::KEY_TASKS = "tasks";
const QString Course::KEY_ANSWER = "answer";
const QString Course::KEY_IMAGES = "images";
const QString Course::KEY_UUID = "uuid";


QString Course::jsonFileName()
{
	return "index.json";
}

Course::Course():
	m_uuid(QUuid::createUuid().toString())
{
	
}

Course::Course(QJsonObject json) 
	: m_uuid(
		json.contains(KEY_UUID) ? 
			json.value(KEY_UUID).toString() : 
			QUuid::createUuid().toString()
	)
{
	m_name = json.value(KEY_NAME).toString();
	m_description = json.value(KEY_DESCRIPTION).toString();
	m_imageName = json.value(KEY_IMAGE_NAME).toString();

	QJsonArray jsonTasksArray = json.value(KEY_TASKS).toArray();
	for(QJsonValue jsonTaskValue: jsonTasksArray)
	{
		QJsonObject o = jsonTaskValue.toObject();
		Task task;
		QString answer = o.value(KEY_ANSWER).toString();
		task.setAnswer(answer);
		QJsonArray ar = o.value(KEY_IMAGES).toArray();
		for(QJsonValue a: ar)
		{
			task.addImage(a.toString());
		}
		
		m_tasks.push_back(task);
	}
}

QJsonObject Course::toJson()
{
	QJsonObject result;
	
	result.insert(KEY_NAME, m_name);
	result.insert(KEY_DESCRIPTION, m_description);
	result.insert(KEY_IMAGE_NAME, m_imageName);
	result.insert(KEY_UUID, m_uuid);
	
	QJsonArray array;
	for(Task& task: m_tasks)
	{
		QJsonObject taskJ = task.toJson();
		array.push_back(taskJ);
	}
	
	result.insert(KEY_TASKS, array);
	
	return result;
}

QString Course::name() const
{
	return m_name;
}

void Course::setName(const QString& name)
{
	m_name = name;
}

QString Course::description() const
{
	return m_description;
}

void Course::setDescription(const QString& description)
{
	m_description = description;
}

QString Course::imageName() const
{
	return m_imageName;
}

void Course::setImageName(const QString& imageName)
{
	m_imageName = imageName;
}

int Course::tasksCount() const
{
	return m_tasks.size();
}

const Course::Task& Course::taskAt(int position) const
{
	return m_tasks[position];
}

void Course::removeTaskAt(int position)
{
	m_tasks.removeAt(position);
}

void Course::insert(int position, Course::Task task)
{
	m_tasks.insert(position, task);
}

QStringList Course::tasksName()
{
	QStringList list;
	
	for(Task& task: m_tasks)
	{
		list.push_back(task.answer());
	}
	
	return list;
}

void Course::replaceTask(Course::Task task)
{
	removeTask(task.answer());
	m_tasks.push_back(task);
}

bool Course::removeTask(QString answer)
{
	bool result = false;
	
	int size = m_tasks.size();
	for(int i = 0; i <size; ++i)
	{
		if (m_tasks[i].answer() == answer)
		{
			m_tasks.removeAt(i);
			result = true;
			break;
		}
	}
	
	return result;
}

QString Course::uuid() const
{
	return m_uuid;
}

QString Course::Task::answer() const
{
	return m_answer;
}

void Course::Task::setAnswer(const QString& answer)
{
	m_answer = answer;
}

int Course::Task::getImageCount()
{
	return m_images.size();
}

void Course::Task::addImage(QString path)
{
	m_images.push_back(path);
}

void Course::Task::removeImage(int position)
{
	m_images.removeAt(position);
}

const QStringList Course::Task::getImages() const
{
	return m_images;
}

QJsonObject Course::Task::toJson()
{
	QJsonObject result;
	
	result.insert(KEY_ANSWER, m_answer);
	QJsonArray images;
	for(QString image: m_images)
	{
		images.push_back(image);
	}
	
	result.insert(KEY_IMAGES, images);
	
	return result;
}
