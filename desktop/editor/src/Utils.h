#ifndef UTILS_H
#define UTILS_H

#include <QString>

class Utils
{
public:
	Utils();
	static QString addFileToCourse(QString filePath, QString basePath);
};

#endif // UTILS_H
