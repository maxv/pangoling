#ifndef COURSE_H
#define COURSE_H

#include <QString>
#include <QList>
#include <QJsonObject>

class Course
{
public:
	static QString jsonFileName();
	
	class Task
	{
	public:
		QString answer() const;
		void setAnswer(const QString& answer);
		
		int getImageCount();
		QString getImagePathAt(int position);
		void addImage(QString path);
		void removeImage(int position);
		const QStringList getImages() const;
		QJsonObject toJson();
	private:
		QString m_answer;
		QList<QString> m_images;
	};
	
	Course();
	Course(QJsonObject json);
	
	QJsonObject toJson();
	
	QString name() const;
	void setName(const QString& name);
	
	QString description() const;
	void setDescription(const QString& description);
	
	QString imageName() const;
	void setImageName(const QString& imageName);
	
	int tasksCount() const;
	const Task& taskAt(int position) const;
	void removeTaskAt(int position);
	void insert(int position, Course::Task task);
	
	QStringList tasksName();
	void replaceTask(Course::Task task);
	bool removeTask(QString answer);
	
	QString uuid() const;
private:
	QString m_name;
	QString	m_description;
	QString m_imageName;
	QList<Course::Task> m_tasks;
	const QString m_uuid;
	
	static const QString KEY_NAME;
	static const QString KEY_DESCRIPTION;
	static const QString KEY_IMAGE_NAME;
	static const QString KEY_TASKS;
	static const QString KEY_ANSWER;
	static const QString KEY_IMAGES;
	static const QString KEY_UUID;
};

#include <QMetaType>

Q_DECLARE_METATYPE(Course::Task)


#endif // COURSE_H
