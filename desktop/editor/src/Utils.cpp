#include "Utils.h"

#include <QFileInfo>
#include <QDir>
#include <QFile>

QString Utils::addFileToCourse(QString filePath, QString basePath)
{
	if (!filePath.startsWith(basePath))
	{
		QFileInfo fileInfo{filePath};
		QString name = fileInfo.fileName();
		QString newPath = basePath + QDir::separator() + name;
		for(long i = 0; QFile::exists(newPath); ++i)
		{
			newPath = basePath + QDir::separator() + QString::number(i) + name;
		}
		
		QFile::copy(filePath, newPath);
		filePath = newPath;
	}
	filePath = filePath.remove(basePath);
	
	return filePath;
}
