#ifndef LINT_H
#define LINT_H

#include <QString>
#include <QStringList>

class Course;

class Lint
{
public:
	Lint() = delete;
	class Result
	{
	public:
		QStringList getUnused()
		{
			return m_unused;
		}

		QStringList getNotFound()
		{
			return m_notFound;
		}
	private:
		QStringList m_unused;
		QStringList m_notFound;
		friend class Lint;
	};

	static Lint::Result check(const Course& course, QString path);
};

#endif // LINT_H
