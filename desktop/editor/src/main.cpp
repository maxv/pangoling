#include <iostream>
#include <QApplication>

#include "gui/CourseWidget.h"
#include "Course.h"

int main(int argc, char** argv)
{
	QApplication app{argc, argv};
	
	qRegisterMetaType<Course::Task>();
	
	CourseWidget* mainWidget = new CourseWidget();
	mainWidget->showMaximized();
	
	return app.exec();
}
