#! /bin/bash

# The approx. time to run this script is 145 ms.

# First we extract the private PEM key from the .p12 file:
# openssl pkcs12 -nocerts -passin 'notasecret' -in file.p12 -out ~/google/google.privatekey.pem 
# TODO: replace file name
KEY=$1 
EMAIL=$2

# The fields are ordered by their hash values.
# In Google Client for Java HashMap is used to stack all JSON fields, so String.hashCode() is used for ordering.
header='{"alg":"RS256","typ":"JWT"}'

aud='https://accounts.google.com/o/oauth2/token'
exp=$(date --date='+1 hour' +%s)
iat=$(date +%s)
iss=$EMAIL
scope='https://www.googleapis.com/auth/androidpublisher'

# The fields are ordered by their hash values.
# In Google Client for Java HashMap is used to stack all JSON fields, so String.hashCode() is used for ordering.
claim="{\"aud\":\"$aud\",\"exp\":$exp,\"iat\":$iat,\"iss\":\"$iss\",\"scope\":\"$scope\"}"

#echo "exp = $exp"
#echo "iat = $iat"

header_b64=$(echo -n "$header" | base64 -w 0 | sed 's/+/-/g;s/\//_/g;s/=//g') # base64url
claim_b64=$(echo -n "$claim" | base64 -w 0 | sed 's/+/-/g;s/\//_/g;s/=//g') # base64url
signature_b64=$(echo -n "$header_b64.$claim_b64" | openssl dgst -sha256 -sign $KEY -passin env:PSWD1 | base64 -w 0 | sed 's/+/-/g;s/\//_/g;s/=//g')

jwt=$(echo -n "$header_b64.$claim_b64.$signature_b64")
#echo $jwt

result=$(curl -s -m 60 --data-urlencode grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer --data-urlencode assertion=$jwt https://accounts.google.com/o/oauth2/token)
access_token=$(echo "$result" | grep -oP '"access_token" : "*\K([a-zA-Z0-9\.\-_])*')

echo $access_token # valid for 3600 seconds

#check token https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=XXXXX

#link=https://www.googleapis.com/androidpublisher/v2/applications/org.pangoling.android.app.release/reviews
#some_params="-G -d part=snippet --data-urlencode id=9bZkp7q19f0"
#curl -H "Authorization: Bearer $access_token" $link








