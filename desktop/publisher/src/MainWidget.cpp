#include "MainWidget.h"

#include <QVBoxLayout>
#include <QTextEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>
#include <QLabel>
#include <QSettings>

#include "DefaultPaths.h"

#include "TokenRequestWidget.h"
#include "ApkSigner.h"
#include "TemporaryScript.h"

const char* MainWidget::SETTINGS_REPO_PATH = "mainwidget.repo_path";

MainWidget::MainWidget(QSettings& settings, QWidget *parent) 
	: QWidget(parent)
	, m_settings(settings)
	, m_tokenRequestWidget(new TokenRequestWidget(settings))
	, m_apkSigner(new ApkSigner(settings))
	, m_replies(new QTextEdit)
	, m_repoPath(new QLineEdit)
	, m_packageName("org.pangoling.android.app.release")
{
	QVBoxLayout* mainLayout = new QVBoxLayout;
	this->setLayout(mainLayout);
	
	QPushButton* btnSetRepo = new QPushButton("Select");
	connect(btnSetRepo, &QPushButton::clicked, this, &MainWidget::onSetRepo);
	
	QHBoxLayout* repoLayout = new QHBoxLayout;
	repoLayout->addWidget(new QLabel("Repo:"));
	repoLayout->addWidget(m_repoPath);
	repoLayout->addWidget(btnSetRepo);
	
	QPushButton* loadAll = new QPushButton("Load all");
	
	mainLayout->addLayout(repoLayout);
	mainLayout->addWidget(m_apkSigner);
	mainLayout->addWidget(m_tokenRequestWidget);
	mainLayout->addWidget(m_replies);
	mainLayout->addWidget(loadAll);
	
	connect(
		m_tokenRequestWidget, &TokenRequestWidget::onTokenUpdated, 
		this, &MainWidget::onTokenUpdated
	);
	
	connect(
		loadAll, &QPushButton::clicked,
		this, &MainWidget::loadAll
	);
	
	setRepoPath(m_settings.value(SETTINGS_REPO_PATH).toString());
	m_repoPath->setEnabled(false);
}

void MainWidget::onTokenUpdated(QString token)
{
	setWindowTitle(token);
	TemporaryScript::loadAllReviews(m_replies, token, m_packageName);
}

void MainWidget::onSetRepo()
{
	QString path = QFileDialog::getExistingDirectory(this, "Repo", BASE_PATH);
	setRepoPath(path);
}

void MainWidget::setRepoPath(QString path)
{
	m_repoPath->setText(path);
	m_tokenRequestWidget->setScriptFile(path + "/" + DefaultPaths::TOKEN_SCRIPT);
	m_apkSigner->setRepoPath(path);
	
	m_settings.setValue(SETTINGS_REPO_PATH, path);
}

#include <QDateTime>
#include <QDate>
#include "Git.h"
#include <QProcess>
#include <QDebug>

void MainWidget::loadAll()
{
	QString branchName = Git::getCurrentBranchName(m_repoPath->text());
	QStringList list = branchName.split('-');
	if (list.at(1) == QString("release"))
	{
		bool commited = TemporaryScript::loadAll(
			m_replies, 
			m_tokenRequestWidget->token(), 
			m_apkSigner->signedApkPath(),
			m_packageName
		);
		
		if (commited)
		{
		
			QDate dt =  QDateTime::currentDateTime().date();
			dt.day();
			dt.month();
			dt.year();
			
			QString tagName = QString("release-%1-%2%3%4")
				.arg(list.at(2))
				.arg(dt.year())
				.arg(dt.month(), 2, '0')
				.arg(dt.day());
			
			QString tagText = QString("We should place release notes here");
			
			Git::putTag(m_repoPath->text(), tagName, tagText);
			
			QProcess p;
			p.setWorkingDirectory(m_repoPath->text());
			p.execute("git", QStringList{"push", "--tags"});
			while(!p.waitForFinished());
			qDebug() << p.readAll();
		}
	}
	else
	{
		qDebug() << "Wrong branch name";
	}
}

