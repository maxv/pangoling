#ifndef TEMPORARYSCRIPT_H
#define TEMPORARYSCRIPT_H

class QTextEdit;
class QString;

class TemporaryScript
{
public:
	TemporaryScript() = delete;
	
	static bool loadAll(
		QTextEdit* replies, 
		const QString& token, 
		const QString& apkPath,
		const QString& packageName
	);
	
	static void loadAllReviews(
		QTextEdit* replies, 
		const QString& token,
		const QString& packageName
	);
};

#endif // TEMPORARYSCRIPT_H
