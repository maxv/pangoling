#ifndef APKSIGNER_H
#define APKSIGNER_H

#include <QGroupBox>

class QLineEdit;
class QSettings;
class QProcess;

class ApkSigner : public QGroupBox
{
	Q_OBJECT
public:
	explicit ApkSigner(QSettings& settings, QWidget *parent = 0);
	~ApkSigner();
	
	QString signedApkPath();
signals:
	
public slots:
	void signApk();
	void setRepoPath(QString path);
private slots:
	void onSignProcessFinishedWithCode(int code);
private:
	void clearSignProcess();
	
	QSettings& m_settings;
	QProcess* m_signProcess;
	QLineEdit* m_keyPath;
	QLineEdit* m_sdkPath;
	QLineEdit* m_password;
	QString m_repoPath;
	
	QString m_signedApkPath;
	
	static const char* SETTINGS_SDK_PATH;
	static const char* SETTINGS_KEY_PATH;
	
	
};

#endif // APKSIGNER_H
