#include "Application.h"

#include <git2.h>

#include "MainWidget.h"

Application::Application(int& argc, char** argv)
	: QApplication(argc, argv)
	, m_settings{"pangoling.org", "publisher"}
{
	git_libgit2_init();
	
	m_mainWidget = new MainWidget(m_settings);
	m_mainWidget->show();
}

Application::~Application()
{
	delete m_mainWidget;
	git_libgit2_shutdown();
}
