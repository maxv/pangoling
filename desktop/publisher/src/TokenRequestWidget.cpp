#include "TokenRequestWidget.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QSettings>

#include <QMetaMethod>

#include <QFileDialog>

#include "TokenGenerator.h"

const char* TokenRequestWidget::SETTINGS_KEY_PATH = "main.key";
const char* TokenRequestWidget::SETTINGS_SCRIPT_PATH = "main.script_path";
const char* TokenRequestWidget::SETTINGS_EMAIL = "main.email";
const bool TokenRequestWidget::SHOW_SCRIPT_LAYOUT = false;

TokenRequestWidget::TokenRequestWidget(QSettings& settings, QWidget *parent) 
	: QGroupBox("TokenRequest", parent)
	, m_settings(settings)
	, m_password(new QLineEdit)
	, m_email(new QLineEdit)
	, m_keyFilePath(new QLineEdit)
	, m_scriptPath(new QLineEdit)
{
	QVBoxLayout* mainLayout = new QVBoxLayout;
	this->setLayout(mainLayout);
	
	QHBoxLayout* passwordLayout = generateLayoutForValue(
		"Password:",
		m_password, 
		"Apply", 
		SLOT(onRegenerateToken())
	);
	mainLayout->addLayout(passwordLayout);
	
	QHBoxLayout* emailLayout = generateLayoutForValue(
		"E-Mail:",
		m_email, 
		"Apply", 
		SLOT(onRegenerateToken())
	);
	mainLayout->addLayout(emailLayout);
	
	QHBoxLayout* keyFileLayout = generateLayoutForValue(
		"Key file:",
		m_keyFilePath, 
		"Select...", 
		SLOT(onSelectKeyFile())
	);
	mainLayout->addLayout(keyFileLayout);
	
	QHBoxLayout* scriptLayout = generateLayoutForValue(
		"Script path <usually already good>:",
		m_scriptPath, 
		"Select...", 
		SLOT(onSelectScriptFile())
	);
	
	if (SHOW_SCRIPT_LAYOUT)
	{
		mainLayout->addLayout(scriptLayout);
	}
	
	loadSettings();
}

QHBoxLayout* TokenRequestWidget::generateLayoutForValue(
	const QString& title,
	QLineEdit* textEdit, 
	const QString& buttonCaption, 
	const char* method
) {
	QPushButton* btn = new QPushButton(buttonCaption);
	QObject::connect(btn, SIGNAL(clicked(bool)), this, method);
	
	QHBoxLayout* result = new QHBoxLayout;
	result->addWidget(new QLabel(title));
	result->addWidget(textEdit);
	result->addWidget(btn);
	
	return result;
}

QString TokenRequestWidget::token()
{
	return m_token;
}

void TokenRequestWidget::setScriptFile(QString path)
{
	m_scriptPath->setText(path);
}

void TokenRequestWidget::onSelectKeyFile()
{
	QString keyFileName = QFileDialog::getOpenFileName(this, "Select key file", "", "*.pem");
	m_keyFilePath->setText(keyFileName);
}

void TokenRequestWidget::onSelectScriptFile()
{
	QString scriptFileName = QFileDialog::getOpenFileName(
		this, 
		"Select script file", 
		BASE_PATH, 
		"*.sh"
	);
	
	setScriptFile(scriptFileName);
}



void TokenRequestWidget::onRegenerateToken()
{
	const QString keyPaht = m_keyFilePath->text();
	const QString password = m_password->text();
	const QString scrpitPath = m_scriptPath->text();
	const QString email = m_email->text();
	
	
	TokenGenerator tg{
		password, 
		email,
		scrpitPath, 
		keyPaht
	};
	
	QString token = tg.generate();
	setWindowTitle(token);
	
	if (!token.isEmpty()) 
	{
		m_settings.setValue(SETTINGS_KEY_PATH, keyPaht);
		m_settings.setValue(SETTINGS_SCRIPT_PATH, scrpitPath);
		m_settings.setValue(SETTINGS_EMAIL, email);
	}
	
	m_token = token;
	emit onTokenUpdated(token);
}

void TokenRequestWidget::loadSettings()
{
	QString keyPath = m_settings.value(SETTINGS_KEY_PATH).toString();
	m_keyFilePath->setText(keyPath);
	
	QString email = m_settings.value(SETTINGS_EMAIL).toString();
	m_email->setText(email);
	
	QVariant scriptPathVariant = m_settings.value(SETTINGS_SCRIPT_PATH);
	if (scriptPathVariant.isNull()) 
	{
		QString def = QString("%1/desktop/publisher/script_from_net.sh").arg(BASE_PATH);
		m_scriptPath->setText(def);
	}
	else
	{
		QString scrpitPath = scriptPathVariant.toString();
		m_scriptPath->setText(scrpitPath);
	}
}

