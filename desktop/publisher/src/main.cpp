#include "Application.h"

#include "MainWidget.h"

int main(int argc, char** argv)
{
	return Application{argc, argv}.exec();
}
