#ifndef TOKENGENERATOR_H
#define TOKENGENERATOR_H

#include <QString>

class TokenGenerator
{
public:
	TokenGenerator(
		const QString& password, 
		const QString& email,
		const QString& scriptPath, 
		const QString& pathToFile
	);
	
	QString generate();
private:
	const QString m_password;
	const QString m_email;
	const QString m_scriptPath;
	const QString m_pathToFile;
};

#endif // TOKENGENERATOR_H
