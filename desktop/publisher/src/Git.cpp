#include "Git.h"

#include <git2.h>

#include <QDebug>

namespace git {
	class Repository
	{
	public:
		Repository(QString path)
		{
			git_repository* repository;
			int result = git_repository_open(
				&repository,
				path.toUtf8().data()
			);
			
			if (result == 0)
			{
				m_repository = repository;
			}
		}
		
		~Repository()
		{
			if (m_repository != nullptr)
			{
				git_repository_free(m_repository);
			}
		}
		
		bool isValid()
		{
			return m_repository != nullptr;
		}
		
		bool createTag(QString tag, QString text)
		{
			git_oid oid;
			
			git_object* target;
			git_revparse_single(&target, m_repository, "HEAD");
			
			git_signature* signature;
			git_signature_default(&signature, m_repository);
			
			int result = git_tag_create(
				&oid, 
				m_repository, 
				tag.toUtf8().data(), 
				target, 
				signature, 
				text.toUtf8().data(),
				0
			);
			
			qDebug() << result;
			
			return result == GIT_EINVALIDSPEC;
		}

	private:
		git_repository* m_repository = nullptr;
		
	};
}

QString Git::getCurrentBranchName(QString path)
{
	QString result;
	
	git_repository* gitRepository;
	int openResult = git_repository_open(&gitRepository, path.toUtf8().data());
	if (openResult == 0)
	{
		git_branch_iterator* branchIterator = nullptr;
		
		int branchResult = git_branch_iterator_new(
			&branchIterator, 
			gitRepository, 
			GIT_BRANCH_LOCAL
		);
		
		if (branchResult == 0)
		{
			git_reference* ref;
			git_branch_t type;
			int next = git_branch_next(&ref, &type, branchIterator);
			while(next == 0)
			{
				int isHead = git_branch_is_head(ref);
				const char* out;
				git_branch_name(&out, ref);
				QString name = out;
				if (isHead == 1)
				{
					result = name;
					break;
				}
				
				next = git_branch_next(&ref, &type, branchIterator);
			}
			
			git_branch_iterator_free(branchIterator);
		}
		else
		{
			qDebug() << "branchResult:" << branchResult;
		}
		
		git_repository_free(gitRepository);
	}
	else
	{
		qDebug() << "openResult:" << openResult;
	}
	
	return result;
}

bool Git::putTag(QString path, QString tag, QString message)
{
	bool result = false;
	
	git::Repository r{path};
	if (r.isValid())
	{
		result = r.createTag(tag, message);
	}
	
	return result;
}
