#ifndef LISTOF_H
#define LISTOF_H

#include <QJsonObject>
#include <QJsonValue>
#include <QJsonDocument>

#include <QMap>
#include <QJsonArray>

template<typename T, typename F, F (T::*K)() const>
class ListOf
{
public:
	typedef QMap<F, T> ValuesByLanguage;
	ListOf(QString s)
	{
		QJsonDocument doc = QJsonDocument::fromJson(s.toUtf8());
		QJsonArray allListings = doc.object().value(T::list_name).toArray();
		for(const QJsonValue& o: allListings)
		{
			T t{o.toObject()};
			m_values.insert((t.*K)(), t);
		}
	}
	
	const ValuesByLanguage& values()
	{
		return m_values;
	}

private:
	ValuesByLanguage m_values;
};
#endif // LISTOF_H
