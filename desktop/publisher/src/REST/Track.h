#ifndef TRACK_H
#define TRACK_H

#include <QString>
#include <QSet>

class QJsonObject;

class Track
{
public:
	class Names {
	public:
		Names() = delete;
		
		static const char* ALPHA;
		static const char* BETA;
		static const char* PRODUCTION;
		static const char* ROLLOUT;
	};
	
	static const char* list_name;
	

	Track(QString json);
	Track(const QJsonObject& o);
	Track();
	
	QByteArray toJsonString();
	
	QString trackName() const;
	void setTrackName(const QString& trackName);
	
	QSet<int> versionCodes() const;
	void setVersionCodes(const QSet<int>& versionCodes);
	
	double userFraction() const;
	void setUserFraction(double userFraction);
	
private:
	QString m_trackName;
	QSet<int> m_versionCodes;
	double m_userFraction;
};

#endif // TRACK_H
