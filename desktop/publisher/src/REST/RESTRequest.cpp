#include "RESTRequest.h"

#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <QApplication>

RESTRequest::RESTRequest(QString path, QString token) 
	: m_path(path)
	, m_token(token)
{
	
}

RESTRequest::RESTRequest(QUrl url, QString token)
	: m_token(token)
	, m_isUrl(true)
	, m_url(url)
{
	
}

#include <QHttpMultiPart>
#include <QHttpPart>

QString RESTRequest::request(RequestType type, QIODevice* iodevice)
{
	QHttpMultiPart mp;
	QHttpPart part;
	
	QUrl url;
	if (m_isUrl)
	{
		qDebug() << m_url;
		url = m_url;
	}
	else
	{
		qDebug() << m_path;
		url = QUrl{m_path};
	}
	
	QNetworkRequest request{url};
	request.setRawHeader(
		"Authorization", 
		QString("Bearer %1").arg(m_token).toUtf8()
	);
	
	QNetworkReply* reply = nullptr;
	switch (type) {
		case RESTRequest::GET:
			reply = m_manager.get(request);
			break;
		case RESTRequest::POST:
			if (iodevice != nullptr)
			{
				part.setHeader(QNetworkRequest::ContentTypeHeader, "application/vnd.android.package-archive");
				part.setBodyDevice(iodevice);
				mp.append(part);
				reply = m_manager.post(request, &mp);
			}
			else
			{
				reply = m_manager.post(request, QByteArray());
			}
			break;
		case RESTRequest::PUT:
			if (iodevice != nullptr)
			{
				request.setRawHeader("Content-Type", "application/json");
				reply = m_manager.put(request, iodevice);
				
//				part.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
//				part.setBodyDevice(iodevice);
//				mp.append(part);
//				reply = m_manager.post(request, &mp);
			}
			else
			{
				reply = m_manager.put(request, QByteArray());
			}
		break;
		default:
			return ""; // TODO: fix it
			break;
	}
	
	while(!reply->waitForReadyRead(300000))
	{
		if (reply->isFinished()) 
		{
			qDebug() << "WTF??";
			break;
		}
		
		QApplication::processEvents();
	}
	
	qDebug() << "!!! error:" << reply->error();
	
	
	QString s = reply->readAll();
	
	qDebug() << s;
	
	return s;
}
