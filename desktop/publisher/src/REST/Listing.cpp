#include "Listing.h"

#include <QJsonDocument>
#include <QJsonObject>

const char* Listing::list_name = "listings";

Listing::Listing(QString s) : Listing(QJsonDocument::fromJson(s.toUtf8()).object())
{
}

Listing::Listing(const QJsonObject& object) 
{
	m_language = object.value("language").toString();
	m_title = object.value("title").toString();
	m_fullDescription = object.value("fullDescription").toString();
	m_shortDescription = object.value("shortDescription").toString();
	m_video = object.value("video").toString();
}

QString Listing::language() const
{
	return m_language;
}

QString Listing::fullDescription() const
{
	return m_fullDescription;
}

QString Listing::shortDescription() const
{
	return m_shortDescription;
}
