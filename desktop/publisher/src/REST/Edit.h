#ifndef EDIT_H
#define EDIT_H

#include <QString>

class Edit
{
public:
	Edit(QString s);
	QString id();

private:
	QString m_id;
	int m_expireTime;
};

#endif // EDIT_H
