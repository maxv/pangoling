#include "Edit.h"

#include <QJsonDocument>
#include <QJsonObject>

Edit::Edit(QString s)
{
	QJsonDocument doc = QJsonDocument::fromJson(s.toUtf8());
	QJsonObject o = doc.object();
	m_id = o.take("id").toString();
	m_expireTime = o.take("expiryTimeSeconds").toInt();
}

QString Edit::id() 
{
	return m_id;
}
