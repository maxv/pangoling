#ifndef RESTREQUEST_H
#define RESTREQUEST_H

#include <QString>
#include <QtNetwork/QNetworkAccessManager>
#include <QUrl>

class RESTRequest
{
public:
	enum RequestType
	{
		GET,
		POST,
		PUT
	};

	RESTRequest(QString path, QString token);
	RESTRequest(QUrl url, QString token);
	QString request(RESTRequest::RequestType type = GET, QIODevice* iodevice = nullptr);
	
	template<typename T>
	T requestAs(RESTRequest::RequestType type = GET, QIODevice* iodevice = nullptr)
	{
		QString s = request(type, iodevice);
		return T(s);
	}

private:
	QNetworkAccessManager m_manager;
	QString m_path;
	QString m_token;
	bool m_isUrl = false;
	QUrl m_url;
};

#endif // RESTREQUEST_H
