#ifndef APK_H
#define APK_H

#include <QString>

class QJsonObject;

class Apk
{
public:
	static const char* list_name;
	
	Apk(QString s);
	Apk(const QJsonObject& o);
	
	int versionCode() const;
private:
	int m_versionCode;
	QString m_sha1;
};
#endif // APK_H
