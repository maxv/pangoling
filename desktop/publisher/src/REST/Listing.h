#ifndef LISTING_H
#define LISTING_H

#include <QString>

class QJsonObject;

class Listing
{
public:
	Listing(QString s);
	Listing(const QJsonObject& object);
	
	QString language() const;
	QString fullDescription() const;
	QString shortDescription() const;
	
	static const char* list_name;
private:
	QString m_language;
	QString m_title;
	QString m_fullDescription;
	QString m_shortDescription;
	QString m_video;
};
#endif // LISTING_H
