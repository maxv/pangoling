#include "Track.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

const char* Track::Names::ALPHA = "alpha";
const char* Track::Names::BETA = "beta";
const char* Track::Names::PRODUCTION = "production";
const char* Track::Names::ROLLOUT = "rollout";

const char* Track::list_name = "tracks";

Track::Track(QString json): Track(QJsonDocument::fromJson(json.toUtf8()).object())
{
	
}

Track::Track(const QJsonObject& o)
{
	m_trackName = o.value("track").toString();
	QJsonArray array = o.value("versionCodes").toArray();
	for(QJsonValue v: array)
	{
		m_versionCodes.insert(v.toInt());
	}
	
	m_userFraction = o.value("userFraction").toDouble();
}

Track::Track()
{
	
}

QByteArray Track::toJsonString()
{
	QJsonObject o;
	o.insert("track", m_trackName);
	QJsonArray array;
	for(int i: m_versionCodes)
	{
		array.append(i);
	}
	o.insert("versionCodes", array);
	o.insert("userFraction", m_userFraction);
	
	QJsonDocument d{o};
	
	return d.toJson();
}

QString Track::trackName() const
{
	return m_trackName;
}

void Track::setTrackName(const QString& trackName)
{
	m_trackName = trackName;
}

QSet<int> Track::versionCodes() const
{
	return m_versionCodes;
}

void Track::setVersionCodes(const QSet<int>& versionCodes)
{
	m_versionCodes = versionCodes;
}

double Track::userFraction() const
{
	return m_userFraction;
}

void Track::setUserFraction(double userFraction)
{
	m_userFraction = userFraction;
}

