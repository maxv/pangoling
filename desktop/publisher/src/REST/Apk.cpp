#include "Apk.h"

#include <QJsonDocument>
#include <QJsonObject>

const char* Apk::list_name = "apks";


Apk::Apk(QString s): Apk(QJsonDocument::fromJson(s.toUtf8()).object())
{
}

Apk::Apk(const QJsonObject& o)
{
	m_versionCode = o.value("versionCode").toInt();
	m_sha1 = o.value("binary").toObject().value("sha1").toString();
}

int Apk::versionCode() const
{
	return m_versionCode;
}
