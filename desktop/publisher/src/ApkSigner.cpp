#include "ApkSigner.h"

#include <QLineEdit>
#include <QProcess>
#include <QStringList>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSettings>
#include <QProcess>

#include "Git.h"
#include "DefaultPaths.h"

#include <QDebug>

const char* ApkSigner::SETTINGS_SDK_PATH = "signer.sdk";
const char* ApkSigner::SETTINGS_KEY_PATH = "signer.key";

ApkSigner::ApkSigner(QSettings& settings, QWidget *parent) 
	: QGroupBox("ApkSigner", parent)
	, m_settings(settings)
	, m_signProcess(nullptr)
	, m_keyPath(new QLineEdit)
	, m_sdkPath(new QLineEdit)
	, m_password(new QLineEdit)
{
	QVBoxLayout* mainLayout = new QVBoxLayout;
	this->setLayout(mainLayout);
	
	QPushButton* btnSign = new QPushButton("Sign!");
	
	connect(
		btnSign, &QPushButton::clicked,
		this, &ApkSigner::signApk
	);
	
	mainLayout->addWidget(m_keyPath);
	mainLayout->addWidget(m_sdkPath);
	mainLayout->addWidget(m_password);
	mainLayout->addWidget(btnSign);
	
	m_keyPath->setText(m_settings.value(SETTINGS_KEY_PATH).toString());
	m_sdkPath->setText(m_settings.value(SETTINGS_SDK_PATH).toString());
}

ApkSigner::~ApkSigner()
{
	clearSignProcess();
}

QString ApkSigner::signedApkPath()
{
	return m_signedApkPath;
}

void ApkSigner::signApk()
{
	QString key = m_keyPath->text();
	QString sdk = m_sdkPath->text();
	QString password = m_password->text();
	QString branch = Git::getCurrentBranchName(m_repoPath);
	
	m_settings.setValue(SETTINGS_KEY_PATH, key);
	m_settings.setValue(SETTINGS_SDK_PATH, sdk);
	m_settings.sync();
	
	clearSignProcess();
	m_signProcess = new QProcess();
	QStringList args;
	args.append(key);
	args.append(sdk);
	args.append(branch);
	args.append(password);
	args.append("silence");
	
	QString scirptPath = m_repoPath + "/" + DefaultPaths::SIGN_SCRIPT;
	
	connect(
		m_signProcess, SIGNAL(finished(int)), 
		this, SLOT(onSignProcessFinishedWithCode(int))
	);
	
	m_signProcess->start(scirptPath, args);
}

void ApkSigner::onSignProcessFinishedWithCode(int code)
{
	if (code == 0)
	{
		m_signedApkPath = m_signProcess->readAllStandardOutput();
		qDebug() << "Path:" << m_signedApkPath;
	}
	else
	{
		QString error = m_signProcess->readAllStandardError();
		qDebug() << "Sign errors:" << error;
	}
	
	clearSignProcess();
}

void ApkSigner::clearSignProcess()
{
	if (m_signProcess != nullptr)
	{
		disconnect(m_signProcess, 0, this, 0);
		m_signProcess->deleteLater();
		m_signProcess = nullptr;
	}
}

void ApkSigner::setRepoPath(QString path)
{
	m_repoPath = path;
}
