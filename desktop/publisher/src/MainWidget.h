#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

class TokenRequestWidget;
class QTextEdit;
class QLineEdit;
class ApkSigner;
class QSettings;

class MainWidget : public QWidget
{
	Q_OBJECT
public:
	explicit MainWidget(QSettings& settings, QWidget *parent = 0);
	
signals:
private slots:
	void onTokenUpdated(QString token);
	void loadAll();
	void onSetRepo();
private:
	void setRepoPath(QString path);
	
	QSettings& m_settings;
	
	TokenRequestWidget* m_tokenRequestWidget;
	ApkSigner* m_apkSigner;
	QTextEdit* m_replies;
	QLineEdit* m_repoPath;
	
	QString m_packageName;
	
	static const char* SETTINGS_REPO_PATH;
};

#endif // MAINWIDGET_H
