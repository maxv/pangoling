#ifndef GIT_H
#define GIT_H

#include <QString>

class Git
{
public:
	Git() = delete;
	static QString getCurrentBranchName(QString path);
	static bool putTag(QString path, QString tag, QString message);
};

#endif // GIT_H
