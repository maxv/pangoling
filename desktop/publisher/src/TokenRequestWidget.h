#ifndef TOKEN_REQUEST_WIDGET_H
#define TOKEN_REQUEST_WIDGET_H

#include <QGroupBox>

class QLineEdit;
class QHBoxLayout;
class QSettings;

class TokenRequestWidget : public QGroupBox
{
	Q_OBJECT
public:
	explicit TokenRequestWidget(QSettings& settings, QWidget *parent = 0);
	QString token();
signals:
	void onTokenUpdated(QString newToken);
public slots:
	void setScriptFile(QString path);
private slots:
	void onSelectKeyFile();
	void onSelectScriptFile();
	void onRegenerateToken();
private:
	QHBoxLayout* generateLayoutForValue(
		const QString& title,
		QLineEdit* textEdit, 
		const QString& buttonCaption, 
		const char* method
	);
	
	void loadSettings();
	
	QSettings& m_settings;
	
	
	QLineEdit* m_password;
	QLineEdit* m_email;
	QLineEdit* m_keyFilePath;
	QLineEdit* m_scriptPath;
	
	QString m_token;
	
	static const char* SETTINGS_KEY_PATH;
	static const char* SETTINGS_SCRIPT_PATH;
	static const char* SETTINGS_EMAIL;
	
	static const bool SHOW_SCRIPT_LAYOUT;
};

#endif // TOKEN_REQUEST_WIDGET_H
