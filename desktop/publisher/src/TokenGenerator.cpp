#include "TokenGenerator.h"

#include <QProcessEnvironment>

#include <QDebug>

TokenGenerator::TokenGenerator(
	const QString& password, 
	const QString& email, 
	const QString& scriptPath, 
	const QString& pathToFile
) : m_password(password)
  , m_email(email)
  , m_scriptPath(scriptPath)
  , m_pathToFile(pathToFile)
{
	
}

QString TokenGenerator::generate()
{
	QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
	env.insert("PSWD1", m_password);
	
	QProcess process;
	QStringList args;
	args.append(m_pathToFile);
	args.append(m_email);
	
	process.setProcessEnvironment(env);
	process.start(m_scriptPath, args);
	while(!process.waitForFinished());
	
	QString s = process.readAllStandardOutput();
	
	qDebug() << "Error:" << process.readAllStandardError();
	
	return s.remove('\n');
}
