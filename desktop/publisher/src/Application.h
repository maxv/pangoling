#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>
#include <QSettings>

class MainWidget;

class Application : public QApplication
{
	Q_OBJECT
public:
	Application(int& argc, char** argv);
	~Application();
private:
	QSettings m_settings;
	MainWidget* m_mainWidget;
};

#endif // APPLICATION_H
