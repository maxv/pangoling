#include "TemporaryScript.h"

#include <QTextEdit>
#include <QString>
#include <QFile>

#include "REST/RESTRequest.h"
#include "REST/Edit.h"
#include "REST/Listing.h"
#include "REST/ListOf.h"
#include "REST/Apk.h"
#include "REST/Track.h"

bool TemporaryScript::loadAll(
	QTextEdit* replies, 
	const QString& token,
	const QString& apkPath,
	const QString& packageName
)
{
	bool result = false;
	
	replies->clear();
	qDebug() << "token:" << token;
	QString editId = 0;
	{
		QString path = "https://www.googleapis.com/androidpublisher/v2/applications/%1/edits";
		QString appPath = path.arg(packageName);
		RESTRequest rq{appPath, token};
		Edit e = rq.requestAs<Edit>(RESTRequest::POST);
		
		replies->append(QString("id:") + e.id());
		editId = e.id();
	}
	
	{
		QString p = "https://www.googleapis.com/androidpublisher/v2/applications/%1/edits/%2/listings";
		RESTRequest rq1{p.arg(packageName).arg(editId), token};
		typedef ListOf<Listing, QString, &Listing::language> ListOfListings;
		ListOfListings al = rq1.requestAs<ListOfListings>(RESTRequest::GET);
		QString s;
		for(const Listing& l: al.values()) 
		{
			QString s1 = QString("%1:%2").arg(l.language()).arg(l.shortDescription());
			s.append("\n").append(s1);
		}
		replies->append(s);
	}
	
	int versionCode = -1;
	{
		QFile f{apkPath};
		f.open(QFile::ReadOnly);
		
		QString p = "https://www.googleapis.com/upload/androidpublisher/v2/applications/%1/edits/%2/apks";
		RESTRequest rq{p.arg(packageName).arg(editId), token};
		Apk apk = rq.requestAs<Apk>(RESTRequest::POST, &f);
		replies->append("\n");
		replies->append(QString("version:%1").arg(apk.versionCode()));
		
		versionCode = apk.versionCode();
	}
	
	{
		typedef ListOf<Apk, int, &Apk::versionCode> ListOfApks;
		QString p = "https://www.googleapis.com/androidpublisher/v2/applications/%1/edits/%2/apks";
		RESTRequest rq{
			p.arg(packageName).arg(editId),
			token
		};
		
		ListOfApks list = rq.requestAs<ListOfApks>(RESTRequest::GET);
		for(Apk t: list.values())
		{
			replies->append(QString("found version:%1").arg(t.versionCode()));
		}
	}
	
	{
		QString p = "https://www.googleapis.com/androidpublisher/v2/applications/%1/edits/%2/tracks";
		RESTRequest rq{
			p.arg(packageName).arg(editId),
			token
		};
		
		typedef ListOf<Track, QString, &Track::trackName> ListOfTracks;
		ListOfTracks list = rq.requestAs<ListOfTracks>(RESTRequest::GET);
		for(Track t: list.values())
		{
			replies->append(t.toJsonString());
		}
	}
	
	{
		Track t;
		t.setTrackName(Track::Names::ALPHA);
		t.setVersionCodes({versionCode});
		t.setUserFraction(0);
		
		replies->append("rq:");
		replies->append(t.toJsonString());
		
		QString s = t.toJsonString();
		qDebug() << s;
		QByteArray data = s.toUtf8();
		
		QString fileName = "/tmp/json.json";
		QFile f(fileName);
		f.open(QFile::WriteOnly);
		f.write(data);
		f.close();
		
		QFile f2(fileName);
		f2.open(QFile::ReadOnly);
		
		QString p = "https://www.googleapis.com/androidpublisher/v2/applications/%1/edits/%2/tracks/%3";
		RESTRequest rq{
			p.arg(packageName).arg(editId).arg(t.trackName()),
			token
		};
		QString response = rq.request(RESTRequest::PUT, &f2);
		replies->append("\n");
		replies->append(response);
	}
	
	{
		QString p = "https://www.googleapis.com/androidpublisher/v2/applications/%1/edits/%2/tracks";
		RESTRequest rq{
			p.arg(packageName).arg(editId),
			token
		};
		
		typedef ListOf<Track, QString, &Track::trackName> ListOfTracks;
		ListOfTracks list = rq.requestAs<ListOfTracks>(RESTRequest::GET);
		for(Track t: list.values())
		{
			replies->append(t.toJsonString());
		}
	}
	
	{
		QString p = "https://www.googleapis.com/androidpublisher/v2/applications/%1/edits/%2:commit";
		RESTRequest rq{
			p.arg(packageName).arg(editId),
			token
		};
		Edit e = rq.requestAs<Edit>(RESTRequest::POST);
		replies->append(QString("id:%1").arg(e.id()));
		
		result = e.id() == editId;
	}
	
	return result;
}

void TemporaryScript::loadAllReviews(
	QTextEdit* replies, 
	const QString& token, 
	const QString& packageName
)
{
	QString path = "https://www.googleapis.com/androidpublisher/v2/applications/%1/reviews";
	
	
	RESTRequest rq{path.arg(packageName), token};
	QString s = rq.request();
	
	replies->append(s);
}

/**
	
**  deobfuscation
	POST https://www.googleapis.com/upload/androidpublisher/v2/applications/packageName/edits/editId/apks/apkVersionCode/deobfuscationFiles/deobfuscationFileType
	
**  details
	GET https://www.googleapis.com/androidpublisher/v2/applications/packageName/edits/editId/details
	PUT https://www.googleapis.com/androidpublisher/v2/applications/packageName/edits/editId/details
	
	{
		"defaultLanguage": string,
		"contactWebsite": string,
		"contactEmail": string,
		"contactPhone": string
	}
	
** images
	GET https://www.googleapis.com/androidpublisher/v2/applications/packageName/edits/editId/listings/language/imageType
	imageType:
		"featureGraphic"
		"icon"
		"phoneScreenshots"
		"promoGraphic"
		"sevenInchScreenshots"
		"tenInchScreenshots"
		"tvBanner"
		"tvScreenshots"
		"wearScreenshots"
	
	{
		"images": [
			{
				"id": string,
				"url": string,
				"sha1": string
			}
		]
	}
	
	POST https://www.googleapis.com/upload/androidpublisher/v2/applications/packageName/edits/editId/listings/language/imageType
	Accepted Media MIME types: image/jpeg, image/png
	
**/
