TARGET=pusblisher
TEMPLATE=app

DEFINES+=BASE_PATH=\\\"$${PWD}/../..\\\"

QT += core gui widgets network

CONFIG += c++11

SOURCES += \
    src/main.cpp \
    src/TokenGenerator.cpp \
    src/TokenRequestWidget.cpp \
    src/MainWidget.cpp \
    src/REST/RESTRequest.cpp \
    src/DefaultPaths.cpp \
    src/REST/Edit.cpp \
    src/REST/Listing.cpp \
    src/Application.cpp \
    src/ApkSigner.cpp \
    src/Git.cpp \
    src/REST/Track.cpp \
    src/REST/Apk.cpp \
    src/TemporaryScript.cpp

HEADERS += \
    src/TokenGenerator.h \
    src/TokenRequestWidget.h \
    src/MainWidget.h \
    src/REST/RESTRequest.h \
    src/DefaultPaths.h \
    src/REST/Edit.h \
    src/REST/Listing.h \
    src/Application.h \
    src/ApkSigner.h \
    src/Git.h \
    src/REST/Track.h \
    src/REST/ListOf.h \
    src/REST/Apk.h \
    src/TemporaryScript.h

OTHER_FILES += \
    script_from_net.sh


unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libgit2
