package org.pangoling.android.app.model.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class TaskImageImpl_isRightSolution_Test {
	@Parameterized.Parameters(name = "{0}")
	public static Collection<TestCase> data() {
		return Arrays.asList(TestCase.values());
	}
	
	public enum TestCase {
		SAME_WORDS("word", "word", true),
		DIFFERENT_WORDS("word", "drow", false),
		CASE_INSENSITIVE("word", "WoRd", true),
		LEADING_SPACES("word", "    word", true),
		ENDING_SPACES("word", "word    ", true),
		LEADING_AND_ENDING_SPACES("word", "    word    ", true),
		;
		
		public final String mAnswer;
		public final String mProposition;
		public final boolean mIsRightAnswer;

		TestCase(String answer, String proposition, boolean isRightAnswer) {
			mAnswer = answer;
			mProposition = proposition;
			mIsRightAnswer = isRightAnswer;
		}
	}
	
	private final TestCase mTestCase;
	
	public TaskImageImpl_isRightSolution_Test(TestCase testCase) {
		mTestCase = testCase;
	}
	
	@Test
	public void check() throws Exception {
		boolean isRight = TaskImageImpl.isRightSolution(mTestCase.mAnswer, mTestCase.mProposition);
		assertEquals(isRight, mTestCase.mIsRightAnswer);
	}

}