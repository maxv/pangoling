package org.pangoling.android.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;
import com.orm.SugarApp;

import org.pangoling.android.app.utils.Logger;

import java.util.concurrent.atomic.AtomicReference;

import okhttp3.OkHttpClient;

public class Application extends SugarApp {
	public static final OkHttpClient DEFAULT_OK_HTTP_CLIENT = new OkHttpClient();
	public static final Gson DEFAULT_GSON = new Gson();
	
	public static final String SHARED_PREFERENCES_NAME = "global.preferences";

	private static final AtomicReference<SharedPreferences> mSharedPreferencesHolder = new AtomicReference<>();
	
	public static SharedPreferences getDefaultPreferences(Context context) {
		synchronized (mSharedPreferencesHolder) {
			if (mSharedPreferencesHolder.get() == null) {
				SharedPreferences sp = context.getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
				mSharedPreferencesHolder.set(sp);
			}
		}
		
		return mSharedPreferencesHolder.get();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Logger.init(this);
		Fresco.initialize(this);
	}
}
