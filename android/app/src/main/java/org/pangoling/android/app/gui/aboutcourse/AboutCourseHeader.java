package org.pangoling.android.app.gui.aboutcourse;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.pangoling.android.app.R;
import org.pangoling.android.app.gui.tasks.CourseTasksActivity;
import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.model.LearningState;
import org.pangoling.android.app.model.TaskImage;
import org.pangoling.android.app.model.ModelAccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.disposables.Disposable;

public class AboutCourseHeader extends CourseTasksAdapter.BaseViewHolder {
	private final long mCourseId;
	
	private final Map<LearningState, LinearLayout.LayoutParams> mLearningStateLayoutParams = new HashMap<>();
	private final LinearLayout mLearningStatesLayout;
	private final ArrayList<Disposable> mDisposables = new ArrayList<>(2);
	
	public static AboutCourseHeader create(ViewGroup parent, CourseInfo ci) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.aboutcourse__header, parent, false);
		return new AboutCourseHeader(view, ci);
	}
	
	private AboutCourseHeader(View itemView, CourseInfo ci) {
		super(itemView);
		mCourseId = ci.getId();
		
		SimpleDraweeView image = (SimpleDraweeView) itemView.findViewById(R.id.aboutcourse__header__course_image);
		image.setImageURI(Uri.parse(ci.getUrl() + ci.getImageName()));

		TextView mStatisticTmp = (TextView) itemView.findViewById(R.id.aboutcourse__header__course_statistic_tmp);
		mStatisticTmp.setText("");

		TextView name = (TextView) itemView.findViewById(R.id.aboutcourse__header__course_name);
		name.setText(ci.getName());

		TextView description = (TextView) itemView.findViewById(R.id.aboutcourse__header__course_description);
		description.setText(ci.getDescription());

		TextView wordsCount = (TextView) itemView.findViewById(R.id.aboutcourse__header__course_words_count);
		Disposable taskCountDisposable = ModelAccess
			.getTasksCount(ci)
			.map(this::mapTaskCountToUIString)
			.subscribe(wordsCount::setText);
		mDisposables.add(taskCountDisposable);
		
		View startButton = itemView.findViewById(R.id.aboutcourse__header__start_course);
		startButton.setOnClickListener(this::startCourse);
		
		mLearningStatesLayout = (LinearLayout) itemView.findViewById(R.id.aboutcourse__header__learning_state_holder);
		for(LearningState learningState: LearningState.values()) {
			View v = new View(mLearningStatesLayout.getContext());
			v.setBackgroundResource(learningState.getColorId());
			
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				0,
				ViewGroup.LayoutParams.MATCH_PARENT
			);
			layoutParams.weight = learningState.ordinal() + 1;
			
			v.setLayoutParams(layoutParams);
			
			mLearningStatesLayout.addView(v);
			
			mLearningStateLayoutParams.put(learningState, layoutParams);
		}

		mLearningStatesLayout.setVisibility(View.INVISIBLE);
		Disposable learningStateDisposable = ModelAccess
			.getCourseLearningState(ci)
			.subscribe(this::applyLearningStates);
		mDisposables.add(learningStateDisposable);
	}

	@Override
	public void set(TaskImage ti) {
		// do nothing
	}
	
	private void startCourse(View v) {
		Context c = v.getContext();
		CourseTasksActivity.startActivity(c, mCourseId);
	}
	
	
	private void applyLearningStates(Map<LearningState, AtomicInteger> learningStateIntegerMap) {
		for(LearningState learningState: LearningState.values()) {
			AtomicInteger value = learningStateIntegerMap.get(learningState);
			LinearLayout.LayoutParams lp = mLearningStateLayoutParams.get(learningState);
			lp.weight = value.get();
		}
		
		mLearningStatesLayout.setVisibility(View.VISIBLE);
		mLearningStatesLayout.requestLayout();
	}

	public Disposable[] getDisposables() {
		Disposable[] result = new Disposable[mDisposables.size()];
		return mDisposables.toArray(result);
	}
	
	private String mapTaskCountToUIString(Long tasksCount) {
		Resources resources = itemView.getContext().getResources(); 
		return resources.getString(
			R.string.course_info_view_holder__tasks_count, 
			tasksCount
		);
	}
}
