package org.pangoling.android.app.gui.aboutapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.pangoling.android.app.R;

public class AboutAppActivity extends AppCompatActivity {
	
	public static void start(Context context) {
		Intent i = new Intent(context, AboutAppActivity.class);
		context.startActivity(i);
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.aboutapp_activity);

		Toolbar toolbar = (Toolbar) findViewById(R.id.aboutapp_activity__toolbar);
		setSupportActionBar(toolbar);
		
		View yandexSpeechKit = findViewById(R.id.aboutapp_activity__yandex_speechkit);
		yandexSpeechKit.setOnClickListener(
			v -> openYandexSpeechKit()
		);
	}
	
	private void openYandexSpeechKit() {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://tech.yandex.ru/speechkit/cloud/"));
		startActivity(browserIntent);
	}
}
