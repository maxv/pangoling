package org.pangoling.android.app;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;

import org.pangoling.android.app.gui.courses.CoursesListActivity;
import org.pangoling.android.app.model.Net2ModelConverter;
import org.pangoling.android.app.network.CourseInfo;
import org.pangoling.android.app.utils.Logger;

import java.util.List;

public class StartActivity extends AppCompatActivity {
	private static final String TAG = StartActivity.class.getName();
	
	private final HandlerThread mBackgroundThread;
	private final Handler mBackgroundThreadHandler;
	{
		mBackgroundThread = new HandlerThread("CourseInfo loader");
		mBackgroundThread.start();
		mBackgroundThreadHandler = new Handler(mBackgroundThread.getLooper());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		mBackgroundThreadHandler.post(mUpdateCoursesInfo);
	}

	@Override
	protected void onDestroy() {
		mBackgroundThreadHandler.removeCallbacks(mUpdateCoursesInfo);
		mBackgroundThread.quitSafely();
		super.onDestroy();
	}

	private final Runnable mUpdateCoursesInfo  = () -> CourseInfo.loadFromWeb(
		getString(R.string.default_repository_path),
		Application.DEFAULT_OK_HTTP_CLIENT,
		Application.DEFAULT_GSON,
		this::onListLoaded,
		this::onLoadingError
	);

	private void onLoadingError(String s, Exception e) {
		if (!isDestroyed()) {
			Logger.logException(TAG, e);
			startCourseListActivity();
		}
	}

	private void startCourseListActivity() {
		CoursesListActivity.startActivity(this);
		finish();
	}

	private void onListLoaded(String url, List<CourseInfo> courseInfos) {
		if (!isDestroyed()) {
			Net2ModelConverter.saveCourseListToDatabase(url, courseInfos);
			startCourseListActivity();
		}
	}
}
