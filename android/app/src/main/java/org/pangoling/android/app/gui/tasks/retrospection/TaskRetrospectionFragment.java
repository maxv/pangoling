package org.pangoling.android.app.gui.tasks.retrospection;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.pangoling.android.app.R;
import org.pangoling.android.app.gui.general.FlatButton;
import org.pangoling.android.app.gui.tasks.TasksAdapter;
import org.pangoling.android.app.model.TaskImage;

import java.util.ArrayList;

public class TaskRetrospectionFragment extends Fragment {
	private static final String ANSWERS = TaskRetrospectionFragment.class.getName() + ".answers";
	private static final String SOLVE_TYPE = TaskRetrospectionFragment.class.getName() + ".solve_type";
	
	private Config mConfig;
	private View mRootView;
	private TextView mAnswers;

	public static class Config {
		@NonNull
		private final TasksAdapter mTasksAdapter;
		
		@NonNull
		private final View.OnClickListener mNextClicked;
		
		@NonNull
		private final View.OnClickListener mTryAgainClicked;
		
		@NonNull
		private final View.OnClickListener mHearClicked;
		
		public Config(
			@NonNull TasksAdapter tasksAdapter,
			@NonNull View.OnClickListener nextClicked,
			@NonNull View.OnClickListener tryAgainClicked,
			@NonNull View.OnClickListener hearClicked
		) {
			mTasksAdapter = tasksAdapter;
			mNextClicked = nextClicked;
			mTryAgainClicked = tryAgainClicked;
			mHearClicked = hearClicked;
		}
	}
	
	public static Fragment create(ArrayList<String> answers, TaskImage.SolveType solveType) {
		TaskRetrospectionFragment fragment = new TaskRetrospectionFragment();
		Bundle args = new Bundle();
		args.putStringArrayList(ANSWERS, answers);
		args.putInt(SOLVE_TYPE, solveType.ordinal());
		fragment.setArguments(args);
		return fragment;
	}
	
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if (mConfig == null) {
			throw new IllegalStateException("Not configured");
		}
		
		mRootView = inflater.inflate(R.layout.task_retrospection_fragment, container, false);
		TaskImage ti = mConfig.mTasksAdapter.getTop();

		TextView answer = (TextView) mRootView.findViewById(R.id.task_retrospection_fragment__right_answer);
		answer.setText(ti.answer());
		
		FlatButton nextWord = (FlatButton) mRootView.findViewById(R.id.task_retrospection_fragment__next_word);
		nextWord.setImageRes(R.drawable.task_retrospection_fragment__next_word);
		nextWord.setOnClickListener(mConfig.mNextClicked);
		
		int solveTypeOrdinal = getArguments().getInt(SOLVE_TYPE);
		TaskImage.SolveType solveType = TaskImage.SolveType.values()[solveTypeOrdinal];
		@DrawableRes final int iconId;
		switch (solveType) {
			case EXACTLY:
			case SO_SIMILAR:
				iconId = R.drawable.solve_type_exactly;
				break;
			case WRONG:
				iconId = R.drawable.solve_type_wrong;
				break;
			default:
				iconId = R.drawable.solve_type_so_similar;
				break;
		}

		View v = mRootView.findViewById(R.id.task_retrospection_fragment__result_image__background);
		v.setBackgroundResource(iconId);
		v.setOnClickListener(p -> showResults());
		
		FlatButton hear = (FlatButton) mRootView.findViewById(R.id.task_retrospection_fragment__hear);
		hear.setImageRes(R.drawable.task_retrospection_fragment__hearing_black);
		hear.setOnClickListener(mConfig.mHearClicked);

		FlatButton tryAgain = (FlatButton) mRootView.findViewById(R.id.task_retrospection_fragment__try_again);
		tryAgain.setImageRes(R.drawable.task_retrospection_fragment__try_again);
		tryAgain.setOnClickListener(mConfig.mTryAgainClicked);
		
		mAnswers = (TextView) mRootView.findViewById(R.id.task_retrospection_fragment__answers);
		
		return mRootView;
	}

	public void setConfig(Config config) {
		mConfig = config;
	}
	
	private void showResults() {
		ArrayList<String> answers = getArguments().getStringArrayList(ANSWERS);
		if (answers != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("\"");
			for (int i = 0; i < answers.size(); ++i) {
				if (i != 0) {
					sb.append("\" or \"");
				}
				sb.append(answers.get(i));
			}
			sb.append("\"");
			
			mAnswers.setText(sb.toString());
			int height = mAnswers.getMeasuredHeight();
			if (mAnswers.getTranslationY() == 0) {
				mAnswers.animate().translationY(-height).start();
			} else {
				mAnswers.animate().translationY(0).start();
			}
		}
	}
}
