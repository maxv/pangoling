package org.pangoling.android.app.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.pangoling.android.app.Application;
import org.pangoling.android.app.R;


public class SettingsActivity extends AppCompatActivity {

	public static void start(Context context) {
		Intent i = new Intent(context, SettingsActivity.class);
		context.startActivity(i);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
			this, 
			LinearLayoutManager.VERTICAL,
			false
		);

		SharedPreferences sp = Application.getDefaultPreferences(this);
		SettingsAdapter settingsAdapter = new SettingsAdapter(sp);
		
		RecyclerView rv = (RecyclerView) findViewById(R.id.settings_activity__recycle_view);
		rv.setLayoutManager(layoutManager);
		rv.setAdapter(settingsAdapter);
	}
}
