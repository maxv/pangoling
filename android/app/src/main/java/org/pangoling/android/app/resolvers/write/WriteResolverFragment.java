package org.pangoling.android.app.resolvers.write;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import org.pangoling.android.app.R;
import org.pangoling.android.app.resolvers.ResolverFragment;

import java.util.ArrayList;

public class WriteResolverFragment extends ResolverFragment {

	public static ResolverActivator createActivator() {
		return new Activator();
	}

	@NonNull
	private EditText mUserInput;

	@Nullable
	@Override
	public View onCreateView(
		LayoutInflater inflater,
		@Nullable ViewGroup container,
		@Nullable Bundle savedInstanceState
	) {
		View result = inflater.inflate(R.layout.write_resolver_fragment, container, false);
		mUserInput = (EditText) result.findViewById(R.id.write_resolver_fragment__input);
		mUserInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					onEditFinished();
				}

				return false;
			}
		});
		result.findViewById(R.id.write_resolver_fragment__ok).setOnClickListener(
			(View v) -> onEditFinished()
		);
		return result;
	}

	@Override
	public void onResume() {
		super.onResume();
		mUserInput.requestFocus();
		InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(mUserInput, InputMethodManager.SHOW_IMPLICIT);
	}

	private void onEditFinished() {
		Context context = getContext();
		if (context != null) {
			View v = getView();
			if (v != null) {
				InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
			}

			String word = mUserInput.getText().toString();
			ArrayList<String> result = new ArrayList<>();
			result.add(word);
			onResult(result);
		}
	}

	private static final class Activator implements ResolverActivator {

		@Override
		public int getName() {
			return R.string.resolver_button_names__write;
		}

		@Override
		public ResolverFragment createFragment() {
			return new WriteResolverFragment();
		}

		@Override
		public int getIconId() {
			return R.drawable.resolver_write;
		}

		@Override
		public boolean isAvailable(Context context) {
			return true;
		}
	}
}
