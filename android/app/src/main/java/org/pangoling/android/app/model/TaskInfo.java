package org.pangoling.android.app.model;

import java.util.List;

public interface TaskInfo {
	String getAnswer();
	List<? extends TaskImage> getImages();
	Long getTaskId();
	CourseInfo getCourse();
}
