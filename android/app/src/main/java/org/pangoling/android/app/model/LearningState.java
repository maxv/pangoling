package org.pangoling.android.app.model;


import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;

import org.pangoling.android.app.R;

public enum LearningState {
	UNKNOWN(
		R.string.learning_state__unknown, 
		R.color.learning_state__unknown
	),
	BAD(
		R.string.learning_state__bad, 
		R.color.learning_state__bad
	),
	MIDDLE(
		R.string.learning_state__middle, 
		R.color.learning_state__middle
	),
	GOOD(
		R.string.learning_state__good, 
		R.color.learning_state__good
	),
	PREFECT(
		R.string.learning_state__perfect, 
		R.color.learning_state__perfect
	);

	public @StringRes int getStringId() {
		return mStringId;
	}
	
	public @ColorRes int getColorId() {
		return mColor;
	}

	private static final int MINIMUM_COUNT_FOR_KNOWN = 5;
	
	private static final int MINIMUM_DIFF_FOR_PERFECT = 10;
	private static final int MINIMUM_DIFF_FOR_GOOD = 5;
	private static final int MINIMUM_DIFF_FOR_MIDDLE = 0;

	public static LearningState getLearningStateFor(TaskImage ti)  {
		int solves = ti.getSolveCount();
		int fails = ti.getFailCount();
		int count = solves + fails;
		int diff = solves - fails;
		final LearningState imageLearningState;
		if (count < 1) {
			imageLearningState = LearningState.UNKNOWN;
		} else if (count < MINIMUM_COUNT_FOR_KNOWN) {
			if (diff > 0) {
				imageLearningState = MIDDLE;
			} else {
				imageLearningState = BAD;
			}
		} else {
			if (diff < MINIMUM_DIFF_FOR_MIDDLE) {
				imageLearningState = LearningState.BAD;
			} else if (diff < MINIMUM_DIFF_FOR_GOOD) {
				imageLearningState = LearningState.MIDDLE;
			} else if (diff < MINIMUM_DIFF_FOR_PERFECT) {
				imageLearningState = LearningState.GOOD;
			} else {
				imageLearningState = LearningState.PREFECT;
			}
		}

		return imageLearningState;
	}
	
	@StringRes
	private final int mStringId;
	
	@ColorRes
	private final int mColor;

	LearningState(@StringRes int stringId, int color) {
		mStringId = stringId;
		mColor = color;
	}
}
