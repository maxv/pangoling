package org.pangoling.android.app.gui.aboutcourse;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.orm.SugarRecord;

import org.pangoling.android.app.R;
import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.model.impl.CourseInfoImpl;
import org.pangoling.android.app.utils.Logger;

public class AboutCourseActivity extends AppCompatActivity {
	private static final String TAG = AboutCourseActivity.class.getName();
	private static final String PARAMETER_COURSE_ID = AboutCourseActivity.class.getName() + ".course_id";
	
	public static void start(Context context, long courseId) {
		Intent i = new Intent(context, AboutCourseActivity.class);
		i.putExtra(PARAMETER_COURSE_ID, courseId);
		context.startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_course);
		
		try {
			CourseInfo ci = getCourseInfo();
			
			RecyclerView courseWords = (RecyclerView) findViewById(R.id.about_course_activity__course_words);
			CourseTasksAdapter adapter = new CourseTasksAdapter(ci);
			courseWords.setAdapter(adapter);
			LinearLayoutManager layoutManager = new LinearLayoutManager(this);
			courseWords.setLayoutManager(layoutManager);
		} catch (IllegalStateException e) {
			Logger.logException(TAG, e);
			finish();
		}
	}

	@NonNull
	private CourseInfo getCourseInfo() throws IllegalStateException {
		CourseInfo result = null;
		
		Intent intent = getIntent();
		
		long courseId = -1;
		if (intent.hasExtra(PARAMETER_COURSE_ID)) {
			courseId = intent.getLongExtra(PARAMETER_COURSE_ID, courseId);
			result = SugarRecord.findById(CourseInfoImpl.class, courseId);
			if (result == null) {
				throw new IllegalStateException("Course not found");
			}
		} else {
			throw new IllegalStateException("CourseId parameter not found");
		}
		
		return result;
	}
}
