package org.pangoling.android.app.gui.aboutcourse;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.pangoling.android.app.R;
import org.pangoling.android.app.gui.TaskImageInfoDialogFragment;
import org.pangoling.android.app.model.LearningState;
import org.pangoling.android.app.model.TaskImage;
import org.pangoling.android.app.model.TaskInfo;

public class TaskViewHolder extends CourseTasksAdapter.BaseViewHolder {
	public static TaskViewHolder create(ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.aboutcourse__task_view_holder, parent, false);
		return new TaskViewHolder(view);
	}
	
	@NonNull
	private final TextView mName;
	
	@NonNull
	private final TextView mStatistic;
	
	private TaskImage mLastTaskImage;
	
	private TaskViewHolder(View itemView) {
		super(itemView);
		mName = (TextView) itemView.findViewById(R.id.aboutcourse__task_view_holder__name);
		mStatistic = (TextView) itemView.findViewById(R.id.aboutcourse__task_view_holder__statistic);
		itemView.setOnClickListener(this::onClick);
	}

	public void set(TaskImage ti) {
		mLastTaskImage = ti;
		mName.setText(ti.answer());
		LearningState ls = LearningState.getLearningStateFor(ti);
		mStatistic.setText(ls.getStringId());

		Resources res = mStatistic.getContext().getResources();
		@ColorInt int color = res.getColor(ls.getColorId());
		mStatistic.setTextColor(color);
	}
	
	public void onClick(View v) {
		TaskInfo taskInfo = mLastTaskImage.getTaskInfo(); 
		long id = taskInfo.getTaskId();
		String baseUrl = taskInfo.getCourse().getUrl();
		TaskImageInfoDialogFragment dialog = TaskImageInfoDialogFragment.create(
			id, 
			mLastTaskImage.getImageName(), 
			baseUrl
		);

		Context c = v.getContext();
		if (c instanceof AppCompatActivity) {
			FragmentManager fm = ((AppCompatActivity)c).getSupportFragmentManager();
			dialog.show(fm, "");
		}
	}
}
