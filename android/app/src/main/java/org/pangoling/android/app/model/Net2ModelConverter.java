package org.pangoling.android.app.model;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

import org.pangoling.android.app.model.impl.CourseInfoImpl;
import org.pangoling.android.app.model.impl.TaskImageImpl;
import org.pangoling.android.app.model.impl.TaskInfoImpl;
import org.pangoling.android.app.network.CourseInfo;

import java.util.ArrayList;
import java.util.List;

public class Net2ModelConverter {

	public static List<Long> saveCourseListToDatabase(
		String url,
		List<org.pangoling.android.app.network.CourseInfo> list
	) {
		ArrayList<Long> result = new ArrayList<>();
		for(CourseInfo ci: list) {
			Long id = saveCourseToDatabase(url, ci);
			result.add(id);
		}

		return result;
	}

	public static Long saveCourseToDatabase(
		@NonNull String url,
		@NonNull org.pangoling.android.app.network.CourseInfo courseInfo
	) {
		CourseInfoImpl courseInfoDb = new CourseInfoImpl(url, courseInfo);
		courseInfoDb.save();

		for(org.pangoling.android.app.network.TaskInfo taskInfo: courseInfo.tasks) {
			TaskInfoImpl taskInfoDb = new TaskInfoImpl(taskInfo, courseInfoDb);
			taskInfoDb.save();

			for(String name: taskInfo.images) {
				TaskImageImpl taskImageDb = new TaskImageImpl(name, taskInfoDb);
				taskImageDb.save();
			}
		}

		return courseInfoDb.getId();
	}
}
