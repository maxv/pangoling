package org.pangoling.android.app.gui;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.pangoling.android.app.R;
import org.pangoling.android.app.model.Statistic;
import org.pangoling.android.app.model.TaskImage;
import org.pangoling.android.app.model.impl.TaskImageImpl;
import org.pangoling.android.app.utils.Logger;

public class TaskImageInfoDialogFragment extends DialogFragment {
	private static final String TAG = TaskImageInfoDialogFragment.class.getName();
	private static final String ARGUMENT_IMAGE_NAME = TaskImageInfoDialogFragment.class.getName() + ".image_name";
	private static final String ARGUMENT_TASK_ID = TaskImageInfoDialogFragment.class.getName() + "task_id";
	private static final String ARGUMENT_BASE_URI = TaskImageInfoDialogFragment.class.getName() + "base_uri";
	
	public static TaskImageInfoDialogFragment create(long taskId, String imageName, String baseUri) {
		TaskImageInfoDialogFragment dialog = new TaskImageInfoDialogFragment();
		
		Bundle arguments = new Bundle();
		arguments.putString(ARGUMENT_IMAGE_NAME, imageName);
		arguments.putString(ARGUMENT_BASE_URI, baseUri);
		arguments.putLong(ARGUMENT_TASK_ID, taskId);
		dialog.setArguments(arguments);
		
		return dialog;
	}
	
	
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(
			R.layout.task_image_info_dialog_fragment, 
			container, 
			false
		);
		
		Bundle arguments = getArguments();
		
		long taskId = arguments.getLong(ARGUMENT_TASK_ID);
		String name = arguments.getString(ARGUMENT_IMAGE_NAME);
		TaskImageImpl taskImage = TaskImageImpl.get(taskId, name);
		
		if (taskImage != null) {
			String baseUri = arguments.getString(ARGUMENT_BASE_URI);
			applyTaskImage(rootView, taskImage, baseUri);
		} else {
			Logger.e(
				TAG, 
				"TaskImageImpl not found for taskId:%s and name:%s", 
				taskId, 
				name
			);
			
			dismiss();
		}
		
		return rootView;
	}
	
	private void applyTaskImage(View rootView, TaskImage taskImage, String baseUri) {
		SimpleDraweeView image = (SimpleDraweeView) rootView.findViewById(R.id.task_image_info_dialog_fragment__image);
		String url = baseUri + "/" + taskImage.getImageName();
		image.setImageURI(Uri.parse(url));

		TextView name = (TextView) rootView.findViewById(R.id.task_image_info_dialog_fragment__answer);
		name.setText(taskImage.answer());
		
		TextView textView = (TextView) rootView.findViewById(R.id.task_image_info_dialog_fragment__statistic);
		Statistic.setStatisticText(textView, taskImage);
	}
}
