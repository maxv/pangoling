package org.pangoling.android.app.model;

import android.widget.TextView;

import org.pangoling.android.app.R;

public enum Statistic { 
	//no instances
	;
	
	public static void setStatisticText(TextView textView, TaskImage taskImage) {
		int failCount = taskImage.getFailCount();
		int solveCount = taskImage.getSolveCount();
		String statisticString = textView.getContext().getString(
			R.string.task_view_holder__statistic,
			solveCount,
			failCount
		);
		textView.setText(statisticString);
	} 
}
