package org.pangoling.android.app.model.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orm.SugarRecord;

import org.pangoling.android.app.model.TaskImage;
import org.pangoling.android.app.model.TaskInfo;

import java.util.Iterator;
import java.util.List;

public class TaskImageImpl extends SugarRecord implements TaskImage {
	public String imageName;

	public TaskInfoImpl taskInfo;

	public int solvedCount;
	public int failCount;

	public TaskImageImpl() {}

	public TaskImageImpl(String name, TaskInfoImpl taskInfoDb) {
		imageName = name;
		taskInfo = taskInfoDb;
	}

	@Override
	public long save() {
		if (getId() == null) {
			TaskImageImpl exist = get(taskInfo.getId(), imageName);
			
			if (exist != null) {
				return exist.getId();
			}
		}
		return super.save();
	}
	
	@Nullable
	public static TaskImageImpl get(Long taskInfoId, String imageName) {
		TaskImageImpl result = null;
		
		List<TaskImageImpl> list = find(
			TaskImageImpl.class,
			"image_name = ? and task_info = ?",
			imageName,
			String.valueOf(taskInfoId)
		);
		
		if (!list.isEmpty()) {
			result = list.get(0);
		}
		
		return result;
	}

	@Override
	public String getImageName() {
		return imageName;
	}

	/*package for tests reasons*/
	static boolean isRightSolution(String rightAnswer, String proposition) {
		return proposition.trim().equalsIgnoreCase(rightAnswer);
	}
	
	@NonNull
	@Override
	public SolveType solve(List<String> propositions) {
		SolveType result = SolveType.WRONG;
		boolean first = true;
		for (String proposition : propositions) {
			boolean isRightSolution = isRightSolution(taskInfo.answer, proposition);
			
			if (isRightSolution) {
				if (first) {
					result = SolveType.EXACTLY;
				} else {
					result = SolveType.SO_SIMILAR;
				}

				break;
			}

			first = false;
		}
		
		switch (result) {
			case EXACTLY:
			case SO_SIMILAR:
				solvedCount += 1;
				break;
			case WRONG:
				failCount += 1;
				break;
		}
		
		save();
		return result;
	}

	@Override
	public int getSolveCount() {
		return solvedCount;
	}

	@Override
	public int getFailCount() {
		return failCount;
	}

	@Override
	public String answer() {
		return taskInfo.answer;
	}

	@Override
	public TaskInfo getTaskInfo() {
		return taskInfo;
	}
}
