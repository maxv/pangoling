package org.pangoling.android.app.utils.tts;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;

import org.pangoling.android.app.Application;
import org.pangoling.android.app.R;
import org.pangoling.android.app.utils.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

public class YandexTextToSpeechHelperFragment extends TextToSpeechHelperFragment {

	private static final String TAG = YandexTextToSpeechHelperFragment.class.getName();
	private static final String PHRASE_PARAMETER = "text";

	@Nullable
	private Call mCurrentDownloadingCall;
	
	@Nullable
	private MediaPlayer mCurrentMediaPlayer;

	@Override
	public void onDestroy() {
		if (mCurrentDownloadingCall != null) {
			mCurrentDownloadingCall.cancel();
			mCurrentDownloadingCall = null;
		}
		
		if (mCurrentMediaPlayer != null) {
			mCurrentMediaPlayer.release();
			mCurrentMediaPlayer = null;
		}
		super.onDestroy();
	}

	@Override
	public void setLanguage(Locale loc) {
		
	}

	@Override
	public void speakWhenReady(String s) {
		speak(s);
	}

	private static HttpUrl buildUrlFor(String s, String key) {
		HttpUrl.Builder urlBuilder = new HttpUrl.Builder();
		urlBuilder.scheme("https");
		urlBuilder.host("tts.voicetech.yandex.net");
		urlBuilder.addEncodedPathSegment("generate");
		urlBuilder.setQueryParameter(PHRASE_PARAMETER, s);
		urlBuilder.setQueryParameter("format", "wav");
		urlBuilder.setQueryParameter("lang", "en-US");
		urlBuilder.setQueryParameter("speaker", "jane");
		urlBuilder.setQueryParameter("key", key);
		
		return urlBuilder.build();
	}

	private static File cacheFileFor(String s, Context c) {
		File cacheDir = c.getCacheDir();
		String fileName = Base64.encodeToString(s.getBytes(), Base64.DEFAULT);
		// TODO: split long file name
		return new File(cacheDir, fileName + ".wav");
	}
	
	private void speak(String s) {
		Context context = getContext();
		if (context != null) {
			File wavFile = cacheFileFor(s, context);
			if (wavFile.exists()) {
				playFile(wavFile);
			} else {
				HttpUrl httpUrl = buildUrlFor(s, context.getString(R.string.ya_speech_kit_key));
				Request.Builder b = new Request.Builder();
				b.url(httpUrl);

				Request request = b.build();

				mCurrentDownloadingCall = Application.DEFAULT_OK_HTTP_CLIENT.newCall(request);
				mCurrentDownloadingCall.enqueue(mLoadingCallback);
			}
		} else {
			Logger.logException(TAG, new IllegalStateException("context is null"));
		}
	}
	
	private void playFile(File f) {
		if (mCurrentMediaPlayer != null) {
			mCurrentMediaPlayer.release();
			mCurrentMediaPlayer = null;
		}
		
		mCurrentMediaPlayer = MediaPlayer.create(getContext(), Uri.fromFile(f));
		try {
			mCurrentMediaPlayer.start();
		} catch (Throwable t) {
			Logger.logException(TAG, t);
		}
	}
	
	@NonNull
	private final Callback mLoadingCallback = new Callback() {
		@Override
		public void onFailure(Call call, IOException e) {
			Logger.logException(TAG, call.request().url().toString(), e);
		}

		@Override
		public void onResponse(Call call, Response response) {
			if (response.isSuccessful()) {
				try {
					String phrase = response.request().url().queryParameter(PHRASE_PARAMETER);
					File wavFile = cacheFileFor(phrase, getContext());

					boolean created = wavFile.createNewFile();
					if (created) {
						byte[] bytes = response.body().bytes();
						OutputStream os = new FileOutputStream(wavFile);
						os.write(bytes);
						os.close();
						playFile(wavFile);
					} else {
						Logger.logException(TAG, new IOException("File not created"));
					}
				} catch (Throwable t) {
					Logger.logException(TAG, t);
				}
			} else {
				Logger.event(Logger.Events.TTS_YA_WRONG_RESPONSE, response.code());
			}
		}
	};
}
