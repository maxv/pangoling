package org.pangoling.android.app.resolvers;

import android.content.Context;

import org.pangoling.android.app.resolvers.pronounce.android.AndroidPronounceResolverFragment;
import org.pangoling.android.app.resolvers.write.WriteResolverFragment;
import org.pangoling.android.app.utils.Logger;

import java.util.ArrayList;
import java.util.List;

public enum ResolverActivatorsFactory {;

	public static List<ResolverFragment.ResolverActivator> getAvailableActivators(Context context) {
		List<ResolverFragment.ResolverActivator> result = new ArrayList<>();

		ResolverFragment.ResolverActivator write = WriteResolverFragment.createActivator();
		if (write.isAvailable(context)) {
			result.add(write);
		}

		ResolverFragment.ResolverActivator pronounce = AndroidPronounceResolverFragment.createActivator();
		if (pronounce.isAvailable(context)) {
			result.add(pronounce);
		} else {
			Logger.event(Logger.Events.GOOGLE_PRONOUNCE_RECOGNITION_NOT_AVAILABLE);
		}

		return result;
	}
}
