package org.pangoling.android.app.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum TaskListGenerator {
	;

	private static final int PREFECT_PROBABILITY = 1;
	private static final int GOOD_PROBABILITY = 3;
	private static final int MIDDLE_PROBABILITY = 6;
	private static final int BAD_PROBABILITY = 12;
	private static final int UNKNOWN_PROBABILITY = 100;

	@NonNull
	public static List<TaskImage> generate(
		@NonNull CourseInfo info,
		int expectedSize
	) {
		List<TaskImage> result = new ArrayList<>();
		List<? extends TaskInfo> taskInfos = info.getTasks();
		List<TaskImage> taskImages = new ArrayList<>();
		for(TaskInfo ti: taskInfos) {
			taskImages.addAll(ti.getImages());
		}

		List<Integer> probabilityes = new ArrayList<>(taskImages.size());
		int sum = 0;
		for(int index = 0; index < taskImages.size(); ++index) {
			TaskImage ti = taskImages.get(index);
			sum += getProbability(ti);
			probabilityes.add(index, sum);
		}

		Random randomGenerator = new Random();
		while(result.size() < expectedSize) {
			int nextRandom = randomGenerator.nextInt(sum);
			int nextElementIndex = Collections.binarySearch(probabilityes, nextRandom);
			if (nextElementIndex < 0) {
				nextElementIndex = -nextElementIndex - 1;
			}

			result.add(taskImages.get(nextElementIndex));
		}

		return result;
	}

	private static int getProbability(TaskImage ti) {
		final int result;

		LearningState learningState = LearningState.getLearningStateFor(ti);
		switch (learningState) {
			case PREFECT:
				result = PREFECT_PROBABILITY;
				break;
			case GOOD:
				result = GOOD_PROBABILITY;
				break;
			case MIDDLE:
				result = MIDDLE_PROBABILITY;
				break;
			case BAD:
				result = BAD_PROBABILITY;
				break;
			case UNKNOWN:
			default:
				result = UNKNOWN_PROBABILITY;
				break;
		}
		return result;
	}
}
