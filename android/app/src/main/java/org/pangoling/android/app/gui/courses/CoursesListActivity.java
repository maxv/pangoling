package org.pangoling.android.app.gui.courses;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.pangoling.android.app.R;
import org.pangoling.android.app.gui.aboutapp.AboutAppActivity;
import org.pangoling.android.app.settings.SettingsActivity;

public class CoursesListActivity extends AppCompatActivity {

	public static void startActivity(Context context) {
		Intent intent = new Intent(context, CoursesListActivity.class);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_courses_list);

		Toolbar toolbar = (Toolbar) findViewById(R.id.courses_list_activity__toolbar);
		setSupportActionBar(toolbar);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_courses_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.course_list_activity__about:
				AboutAppActivity.start(this);
				return true;
			case R.id.course_list_activity__settings:
				SettingsActivity.start(this);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
