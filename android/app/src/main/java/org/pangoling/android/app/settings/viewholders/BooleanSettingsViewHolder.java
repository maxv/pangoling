package org.pangoling.android.app.settings.viewholders;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.pangoling.android.app.R;
import org.pangoling.android.app.settings.Settings;

public class BooleanSettingsViewHolder extends SettingsViewHolder {
	@NonNull
	private final TextView mName;
	
	@NonNull
	private final ImageView mSelectedIcon;
	
	public static class Factory extends SettingsViewHolder.Factory {
		@Override
		public SettingsViewHolder construct(
			@NonNull ViewGroup parent, 
			@NonNull SharedPreferences sp
		) {
			LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
			View view = layoutInflater.inflate(R.layout.boolean_settings_view, parent, false);
			return new BooleanSettingsViewHolder(view, sp);
		}
	}
	
	public BooleanSettingsViewHolder(View itemView, SharedPreferences sharedPreferences) {
		super(itemView, sharedPreferences);
		mName = (TextView) itemView.findViewById(R.id.boolean_settings_view__name);
		mSelectedIcon = (ImageView) itemView.findViewById(R.id.boolean_settings_view__selected_icon); 
		itemView.setOnClickListener(mOnClickListener);
	}

	@Override
	public void bindSettings(Settings settings) {
		mName.setText(settings.name);
		boolean selected = mSharedPreferences.getBoolean(settings.toString(), false);
		applyState(selected);
		mSelectedIcon.setTag(settings);
	}
	
	private void applyState(boolean selected) {
		mSelectedIcon.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
		Context context = mName.getContext();
		if (selected) {
			itemView.setBackgroundResource(R.color.colorPrimary);
			mName.setTextColor(context.getResources().getColor(R.color.white));
		} else {
			itemView.setBackgroundResource(R.drawable.flat_button__background);
			mName.setTextColor(context.getResources().getColor(R.color.primary_text));
		}
	}
	
	private boolean getState() {
		return mSelectedIcon.getVisibility() == View.VISIBLE;
	}
	
	@NonNull
	private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Settings s = (Settings) mSelectedIcon.getTag();
			boolean newState = !getState();
			mSharedPreferences.edit().putBoolean(s.toString(), newState).apply();
			applyState(newState);
		}
	};
}
