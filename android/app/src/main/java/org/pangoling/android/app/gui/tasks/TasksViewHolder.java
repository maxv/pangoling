package org.pangoling.android.app.gui.tasks;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.pangoling.android.app.R;
import org.pangoling.android.app.model.Statistic;
import org.pangoling.android.app.model.TaskImage;

public class TasksViewHolder extends RecyclerView.ViewHolder {
	@NonNull
	private final SimpleDraweeView mDraweeView;
	
	@NonNull
	private final TextView mStatisticText;

	public static TasksViewHolder create(@NonNull ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View itemView = inflater.inflate(R.layout.task_view_holder, parent, false);
		return new TasksViewHolder(itemView);
	}

	public TasksViewHolder(View itemView) {
		super(itemView);
		mDraweeView = (SimpleDraweeView) itemView.findViewById(R.id.task_view_holder__image);
		mStatisticText = (TextView) itemView.findViewById(R.id.task_view_holder__statistic);
	}

	public void bind(TaskImage taskImage, String baseUrl, boolean showStatistic) {
		String url = baseUrl + taskImage.getImageName();
		mDraweeView.setImageURI(
			Uri.parse(
				url
			)
		);
		
		if (showStatistic) {
			Statistic.setStatisticText(mStatisticText, taskImage);
		} else {
			mStatisticText.setText("");
		}
	}
}
