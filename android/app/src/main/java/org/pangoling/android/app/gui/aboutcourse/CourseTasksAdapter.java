package org.pangoling.android.app.gui.aboutcourse;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.model.TaskImage;
import org.pangoling.android.app.model.ModelAccess;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class CourseTasksAdapter extends RecyclerView.Adapter<CourseTasksAdapter.BaseViewHolder> {
	private static final int VIEW_TYPE_HEADER = 0;
	private static final int VIEW_TYPE_ITEM = 1;
	
	public static abstract class BaseViewHolder extends RecyclerView.ViewHolder {
		public BaseViewHolder(View itemView) {
			super(itemView);
		}

		public abstract void set(TaskImage ti);
	}
	
	@NonNull
	private final CourseInfo mCourseInfo;
	
	@NonNull
	private final CompositeDisposable mAllDisposablies = new CompositeDisposable();
	
	@NonNull
	private final List<TaskImage> mTaskInfoList = new ArrayList<>();

	public CourseTasksAdapter(
		@NonNull CourseInfo courseInfo
	) {
		mCourseInfo = courseInfo;
	}

	@Override
	public void onAttachedToRecyclerView(RecyclerView recyclerView) {
		super.onAttachedToRecyclerView(recyclerView);
		
		Disposable disposable = ModelAccess.getImages(mCourseInfo).subscribe(
			this::setImagesList
		);
		mAllDisposablies.add(disposable);
	}

	@Override
	public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
		mAllDisposablies.dispose();
		super.onDetachedFromRecyclerView(recyclerView);
	}

	private void setImagesList(List<TaskImage> images) {
		int size = mTaskInfoList.size();
		mTaskInfoList.clear();
		notifyItemRangeRemoved(1, size);
		
		mTaskInfoList.addAll(images);
		notifyItemRangeInserted(1, mTaskInfoList.size());
	}

	@Override
	public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		switch (viewType) {
			case VIEW_TYPE_HEADER:
				AboutCourseHeader header = AboutCourseHeader.create(parent, mCourseInfo);
				mAllDisposablies.addAll(header.getDisposables());
				return header;
			case VIEW_TYPE_ITEM:
				return TaskViewHolder.create(parent);
			default:
				throw new IllegalArgumentException("Unknown view type:" + viewType);
		}
	}

	@Override
	public void onBindViewHolder(BaseViewHolder holder, int position) {
		if (position > 0) {
			holder.set(mTaskInfoList.get(position - 1));
		}
	}

	@Override
	public int getItemCount() {
		return mTaskInfoList.size() + 1;
	}

	@Override
	public int getItemViewType(int position) {
		return (position == 0) ? VIEW_TYPE_HEADER : VIEW_TYPE_ITEM;
	}
}
