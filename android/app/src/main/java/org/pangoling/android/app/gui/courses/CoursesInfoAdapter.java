package org.pangoling.android.app.gui.courses;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import org.pangoling.android.app.gui.aboutcourse.AboutCourseActivity;
import org.pangoling.android.app.gui.tasks.CourseTasksActivity;
import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.utils.Logger;

import java.util.ArrayList;
import java.util.List;

public class CoursesInfoAdapter extends RecyclerView.Adapter<CourseInfoViewHolder> {
	@NonNull
	private final List<CourseInfo> mCourseInfoList = new ArrayList<>();

	public void setCourseInfoList(@NonNull List<? extends CourseInfo> courseList) {
		int size = mCourseInfoList.size();
		mCourseInfoList.clear();
		notifyItemRangeRemoved(0, size);
		mCourseInfoList.addAll(courseList);
		notifyItemRangeInserted(0, mCourseInfoList.size());
	}

	@Override
	public CourseInfoViewHolder onCreateViewHolder(
		ViewGroup parent,
		int viewType
	) {
		return CourseInfoViewHolder.create(
			parent,
			this::onStartCourseClicked,
			this::onAboutCourseClicked
		);
	}

	@Override
	public void onBindViewHolder(CourseInfoViewHolder holder, int position) {
		CourseInfo courseInfo = mCourseInfoList.get(position);
		holder.set(courseInfo);
	}

	@Override
	public int getItemCount() {
		return mCourseInfoList.size();
	}

	private void onStartCourseClicked(Context context, int position) {
		CourseInfo courseInfo = mCourseInfoList.get(position);
		Long id = courseInfo.getId();
		if (id != null) {
			Logger.event(Logger.Events.COURSE_START, courseInfo.getUrl());
			CourseTasksActivity.startActivity(context, id);
		} else {
			Throwable t = new IllegalStateException("Course id is null");
			Logger.logException(Logger.TAG_NON_FATAL, t);
		}
	}

	private void onAboutCourseClicked(Context context, int position) {
		CourseInfo courseInfo = mCourseInfoList.get(position);
		Long id = courseInfo.getId();
		if (id != null) {
			Logger.event(Logger.Events.COURSE_ABOUT, courseInfo.getUrl());
			AboutCourseActivity.start(context, id);
		} else {
			Throwable t = new IllegalStateException("Course id is null");
			Logger.logException(Logger.TAG_NON_FATAL, t);
		}
	}
}
