package org.pangoling.android.app.gui.courses;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.pangoling.android.app.R;
import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.model.ModelAccess;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class CoursesListFragment extends Fragment {
	@NonNull
	private final CoursesInfoAdapter mCoursesInfoAdapter = new CoursesInfoAdapter();
	
	@NonNull
	private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

	@Nullable
	@Override
	public View onCreateView(
		LayoutInflater inflater,
		@Nullable ViewGroup container,
		@Nullable Bundle savedInstanceState
	) {
		View rootView = inflater.inflate(
			R.layout.course_info_list_fragment,
			container,
			false
		);

		RecyclerView recyclerView = (RecyclerView) rootView.findViewById(
			R.id.course_info_list_fragment__recycle_view
		);
		recyclerView.setAdapter(mCoursesInfoAdapter);

		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
		recyclerView.setLayoutManager(linearLayoutManager);

		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		Disposable courseLoadingDisposable = ModelAccess.getCourses().subscribe(this::onCourseInfoUpdated);
		mCompositeDisposable.add(courseLoadingDisposable);
	}

	@Override
	public void onDetach() {
		mCompositeDisposable.dispose();
		super.onDetach();
	}
	
	private void onCourseInfoUpdated(@io.reactivex.annotations.NonNull List<? extends CourseInfo> courseInfo) {
		mCoursesInfoAdapter.setCourseInfoList(courseInfo);
	}
	
}
