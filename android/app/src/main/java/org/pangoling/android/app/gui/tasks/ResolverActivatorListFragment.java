package org.pangoling.android.app.gui.tasks;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.pangoling.android.app.R;
import org.pangoling.android.app.gui.general.FlatButton;
import org.pangoling.android.app.resolvers.ResolverActivatorViewHolder;
import org.pangoling.android.app.resolvers.ResolverActivatorsAdapter;
import org.pangoling.android.app.resolvers.ResolverActivatorsFactory;
import org.pangoling.android.app.resolvers.ResolverFragment;

import java.util.List;

public class ResolverActivatorListFragment extends Fragment {
	private Config mConfig;
	
	public static class Config {
		@NonNull
		private final ResolverActivatorViewHolder.ResolverActivationListener mResolverActivationListener;
		
		@NonNull
		private final View.OnClickListener mOnUnknown;

		public Config(
			@NonNull ResolverActivatorViewHolder.ResolverActivationListener resolverActivationListener,
			@NonNull View.OnClickListener onUnknown
		) {
			mResolverActivationListener = resolverActivationListener;
			mOnUnknown = onUnknown;
		}
	}
	
	@NonNull
	private final ResolverActivatorViewHolder.ResolverActivationListener mResolverActivationListener = new ResolverActivatorViewHolder.ResolverActivationListener() {
		@Override
		public void onResolverActivated(ResolverFragment f) {
			mConfig.mResolverActivationListener.onResolverActivated(f);
		}
	};
	
	@NonNull
	private final ResolverActivatorsAdapter mResolverActivatorsAdapter = new ResolverActivatorsAdapter(
		new ButtonResolverActivatorViewHolder.Factory(),
		mResolverActivationListener
	);

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		List<ResolverFragment.ResolverActivator> list = ResolverActivatorsFactory.getAvailableActivators(context);
		mResolverActivatorsAdapter.setActivators(list);
	}

	@Nullable
	@Override
	public View onCreateView(
		LayoutInflater inflater, 
		@Nullable ViewGroup container, 
		@Nullable Bundle savedInstanceState
	) {
		
		if (mConfig == null) {
			throw new IllegalStateException("Still not configured");
		}
		View result = inflater.inflate(R.layout.resolver_activator_list_framgnet, container, false);

		RecyclerView resolverActivators = (RecyclerView) result.findViewById(R.id.resolver_activator_list_fragment__recycle_view);
		resolverActivators.setAdapter(mResolverActivatorsAdapter);

		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(inflater.getContext());
		linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		resolverActivators.setLayoutManager(linearLayoutManager);

		FlatButton unknown = (FlatButton) result.findViewById(R.id.resolver_activator_list_fragment__unknown);
		unknown.setImageRes(R.drawable.resolver_activator_list_fragment__unknown);
		unknown.setOnClickListener(mConfig.mOnUnknown);
		
		return result;
	}

	public void setConfig(Config config) {
		mConfig = config;
	}
}
