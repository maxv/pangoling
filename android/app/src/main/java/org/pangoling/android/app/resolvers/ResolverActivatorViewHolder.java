package org.pangoling.android.app.resolvers;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public abstract class ResolverActivatorViewHolder extends RecyclerView.ViewHolder {
	public interface ResolverActivationListener {
		void onResolverActivated(ResolverFragment f);
	}

	public static abstract class Factory {
		@NonNull
		public abstract ResolverActivatorViewHolder construct(
			ViewGroup parent,
			@NonNull ResolverActivationListener listener
		);
	}

	public ResolverActivatorViewHolder(View itemView) {
		super(itemView);
	}

	public abstract void bind(@NonNull ResolverFragment.ResolverActivator activator);
}
