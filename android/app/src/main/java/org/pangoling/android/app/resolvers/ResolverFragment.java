package org.pangoling.android.app.resolvers;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public abstract class ResolverFragment extends Fragment {
	public interface ResultListener {
		void onResult(ArrayList<String> s);
	}

	public interface ResolverActivator {
		@StringRes int getName();
		ResolverFragment createFragment();

		@DrawableRes
		int getIconId();

		boolean isAvailable(Context context);
	}

	@NonNull
	private final List<ResultListener> mListeners = new ArrayList<>();

	public void addListener(@NonNull ResultListener listener) {
		if (!mListeners.contains(listener)) {
			mListeners.add(listener);
		} else {
			throw new IllegalArgumentException("Can't add the same listener second time");
		}
	}

	public boolean removeListener(@NonNull ResultListener listener) {
		return mListeners.remove(listener);
	}

	protected void onResult(ArrayList<String> s) {
		for(ResultListener l: mListeners) {
			l.onResult(s);
		}
	}
}
