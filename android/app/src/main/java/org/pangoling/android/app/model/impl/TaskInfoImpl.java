package org.pangoling.android.app.model.impl;

import android.support.annotation.Nullable;

import com.orm.SugarRecord;

import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.model.TaskInfo;

import java.util.List;

public class TaskInfoImpl extends SugarRecord implements TaskInfo {
	public String answer;
	public CourseInfoImpl courseInfo;

	public TaskInfoImpl() {}

	public TaskInfoImpl(
		org.pangoling.android.app.network.TaskInfo taskInfoNet,
		CourseInfoImpl courseInfoDb
	) {
		answer = taskInfoNet.answer;
		courseInfo = courseInfoDb;
	}

	public List<TaskImageImpl> getAllImages() {
		return TaskImageImpl.find(
			TaskImageImpl.class,
			"task_info = ?",
			String.valueOf(getId())
		);
	}

	@Override
	public long save() {
		if (getId() == null) {
			List<TaskInfoImpl> list = find(
				TaskInfoImpl.class,
				"answer = ? and course_info = ?",
				answer,
				String.valueOf(courseInfo.getId())
			);

			if (!list.isEmpty()) {
				return list.get(0).getId();
			}
		}
		return super.save();
	}

	@Override
	public String getAnswer() {
		return answer;
	}

	@Override
	public List<? extends TaskImageImpl> getImages() {
		return getAllImages();
	}

	@Override
	@Nullable
	public Long getTaskId() {
		return getId();
	}

	@Override
	public CourseInfo getCourse() {
		return courseInfo;
	}
}
