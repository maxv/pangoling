package org.pangoling.android.app.gui.tasks;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.orm.SugarRecord;

import org.pangoling.android.app.Application;
import org.pangoling.android.app.R;
import org.pangoling.android.app.gui.tasks.deck.DeckItemAnimator;
import org.pangoling.android.app.gui.tasks.deck.DeckLayoutManager;
import org.pangoling.android.app.gui.tasks.retrospection.TaskRetrospectionFragment;
import org.pangoling.android.app.resolvers.ResolverActivatorViewHolder;
import org.pangoling.android.app.resolvers.ResolverFragment;
import org.pangoling.android.app.model.TaskImage;
import org.pangoling.android.app.model.impl.CourseInfoImpl;
import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.settings.Settings;
import org.pangoling.android.app.utils.Logger;
import org.pangoling.android.app.utils.tts.TextToSpeechHelperFragment;
import org.pangoling.android.app.utils.tts.YandexTextToSpeechHelperFragment;

import java.util.ArrayList;
import java.util.Locale;

public class CourseTasksActivity extends AppCompatActivity {
	private static final String COURSE_ID = CourseTasksActivity.class + ".course_id";
	private static final String RESOLVER_FRAGMENT_TAG = CourseTasksActivity.class + ".resolver_fragment";
	private static final String TTS_FRAGMENT_TAG = CourseTasksActivity.class.getName() + ".tts_fragment_tag";
	private static final String TAG = CourseTasksActivity.class.getName();

	@IdRes
	private static final int CONTAINER_ID = R.id.course_tasks_activity__resolver_fragment_container;

	public static void startActivity(Context context, long courseId) {
		Intent i = new Intent(context, CourseTasksActivity.class);
		i.putExtra(COURSE_ID, courseId);
		context.startActivity(i);
	}
	
	private Toolbar mToolbar;
	private final TasksAdapter mTasksAdapter = new TasksAdapter();
	private TextToSpeechHelperFragment mTextToSpeechHelperFragment;
	private View mContainer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		setContentView(R.layout.activity_course_tasks);

		mToolbar = (Toolbar) findViewById(R.id.course_tasks_activity__toolbar);
		setSupportActionBar(mToolbar);
		
		SharedPreferences sp = Application.getDefaultPreferences(this);
		boolean showStatistic = sp.getBoolean(
			Settings.SHOW_TASK_STATISTIC.toString(),
			false
		);
		mTasksAdapter.setShowStatistic(showStatistic);
		
		DeckLayoutManager deckLayoutManager = new DeckLayoutManager(this);
		
		RecyclerView tasksRecyclerView = (RecyclerView) findViewById(R.id.course_tasks_activity__images_recycler);
		tasksRecyclerView.setAdapter(mTasksAdapter);
		tasksRecyclerView.setItemAnimator(new DeckItemAnimator());
		tasksRecyclerView.setLayoutManager(deckLayoutManager);

		ItemTouchHelper ith = new ItemTouchHelper(mCallback);
		ith.attachToRecyclerView(tasksRecyclerView);
		
		if (savedInstanceState == null) {
			ResolverActivatorListFragment f = new ResolverActivatorListFragment();
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.add(R.id.course_tasks_activity__resolver_fragment_container, f);
			ft.commit();
		}

		Intent intent = getIntent();
		if (intent.hasExtra(COURSE_ID)) {
			long courseId = intent.getLongExtra(COURSE_ID, -1);
			CourseInfo courseInfo = SugarRecord.findById(CourseInfoImpl.class, courseId);
			setCourse(courseInfo);
		} else {
			finish();
		}
		
		if (mTextToSpeechHelperFragment == null) {
			Fragment f = new YandexTextToSpeechHelperFragment();
			// Fragment f = new AndroidTextToSpeechHelperFragment();
			getSupportFragmentManager().beginTransaction().add(f, TTS_FRAGMENT_TAG).commit();
		}
		
		mContainer = findViewById(CONTAINER_ID);
		mTasksAdapter.registerAdapterDataObserver(mAdapterDataObserver);
		onAdapterSizeChanged();
	}

	@Override
	protected void onDestroy() {
		mTasksAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
		super.onDestroy();
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		super.onAttachFragment(fragment);
		if (fragment instanceof ResolverFragment) {
			ResolverFragment resolverFragment = (ResolverFragment) fragment;
			resolverFragment.addListener(mResolverResultListener);
		} else if (fragment instanceof ResolverActivatorListFragment) {
			ResolverActivatorListFragment activatorList = (ResolverActivatorListFragment) fragment;
			activatorList.setConfig(
				new ResolverActivatorListFragment.Config(
					mResolverActivationListener,
					mShowUnknown
				)
			);
		} else if (fragment instanceof TaskRetrospectionFragment) {
			TaskRetrospectionFragment retrospectionFragment = (TaskRetrospectionFragment) fragment;
			retrospectionFragment.setConfig(
				new TaskRetrospectionFragment.Config(
					mTasksAdapter,
					mNextTask,
					mTryAgain,
					mHearCurrent
				)
			);
		} else if (fragment instanceof TextToSpeechHelperFragment) {
			if (mTextToSpeechHelperFragment != null) {
				Logger.logException(TAG, new IllegalStateException("mTextToSpeechHelperFragment != null"));
			}
			mTextToSpeechHelperFragment = (TextToSpeechHelperFragment) fragment;
			mTextToSpeechHelperFragment.setLanguage(Locale.ENGLISH);
		}
	}

	private void setCourse(CourseInfo course) {
		mTasksAdapter.setCourse(course);
		mToolbar.setTitle(course.getName());
		setTitle(course.getName());
	}

	private ItemTouchHelper.Callback mCallback = new ItemTouchHelper.Callback() {
		@Override
		public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
			int dragFlags = 0;
			int swipeFlags = 0;

			int position = viewHolder.getAdapterPosition();
			if (position == 0) {
				swipeFlags = ItemTouchHelper.START |
					ItemTouchHelper.END |
					ItemTouchHelper.UP |
					ItemTouchHelper.DOWN;
			}

			return makeMovementFlags(dragFlags, swipeFlags);
		}

		@Override
		public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
			return false;
		}

		@Override
		public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
			mTasksAdapter.onRemoved((TasksViewHolder)viewHolder);
		}

		@Override
		public boolean isItemViewSwipeEnabled() {
			return true;
		}

		@Override
		public boolean isLongPressDragEnabled() {
			return false;
		}
	};

	@NonNull
	private final ResolverFragment.ResultListener mResolverResultListener = resolverResult -> {
		TaskImage ti = mTasksAdapter.getTop();

		TaskImage.SolveType solveType = ti.solve(resolverResult);
		Logger.event(Logger.Events.ANSWER_RESULT, solveType);

		getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		
		Fragment f = TaskRetrospectionFragment.create(resolverResult, solveType);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(CONTAINER_ID, f);
		ft.addToBackStack(null);
		ft.commitAllowingStateLoss();
	};

	private final View.OnClickListener mNextTask = v -> {
		mTasksAdapter.removeTop();
		getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	};
	
	private final View.OnClickListener mHearCurrent = v -> {
		if (mTextToSpeechHelperFragment != null) {
			mTextToSpeechHelperFragment.speakWhenReady(mTasksAdapter.getTop().answer());
		}
	};

	private final View.OnClickListener mTryAgain = v -> 
		getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

	private View.OnClickListener mShowUnknown = v -> {
		Fragment f = TaskRetrospectionFragment.create(new ArrayList<>(), TaskImage.SolveType.WRONG);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(CONTAINER_ID, f);
		ft.addToBackStack(null);
		ft.commitAllowingStateLoss();
	};
	
	private void onAdapterSizeChanged() {
		if (mTasksAdapter.getItemCount() > 0) {
			mContainer.setVisibility(View.VISIBLE);
		} else {
			mContainer.setVisibility(View.INVISIBLE);
		}
	}
	
	@NonNull
	private final ResolverActivatorViewHolder.ResolverActivationListener mResolverActivationListener = new ResolverActivatorViewHolder.ResolverActivationListener() {
		@Override
		public void onResolverActivated(ResolverFragment f) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.replace(CONTAINER_ID, f, RESOLVER_FRAGMENT_TAG);
			ft.addToBackStack(null);
			ft.commitAllowingStateLoss();
		}
	};
	
	private RecyclerView.AdapterDataObserver mAdapterDataObserver = new RecyclerView.AdapterDataObserver() {
		@Override
		public void onChanged() {
			super.onChanged();
			onAdapterSizeChanged();
		}

		@Override
		public void onItemRangeChanged(int positionStart, int itemCount) {
			super.onItemRangeChanged(positionStart, itemCount);
			onAdapterSizeChanged();
		}

		@Override
		public void onItemRangeInserted(int positionStart, int itemCount) {
			super.onItemRangeInserted(positionStart, itemCount);
			onAdapterSizeChanged();
		}

		@Override
		public void onItemRangeRemoved(int positionStart, int itemCount) {
			super.onItemRangeRemoved(positionStart, itemCount);
			if (positionStart == 0) {
				getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}

			onAdapterSizeChanged();
		}

		@Override
		public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
			super.onItemRangeMoved(fromPosition, toPosition, itemCount);
		}
	};
}
