package org.pangoling.android.app.model;

import java.util.List;

public interface CourseInfo {
	Long getId();
	String getName();
	String getDescription();
	String getUrl();
	String getImageName();

	List<? extends TaskInfo> getTasks();
}
