package org.pangoling.android.app.gui.general;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import org.pangoling.android.app.R;

public class FlatButton extends AppCompatButton {
	public FlatButton(Context context) {
		super(context);
	}

	public FlatButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FlatButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void setImageRes(@DrawableRes int image) {
		Context context = getContext();
		Drawable drawable = context.getDrawable(image);
		if (drawable != null) {
			int size = (int) context.getResources().getDimension(R.dimen.flat_button__image_size);
			drawable.setBounds(0, 0, size, size);
			super.setCompoundDrawablePadding(-size);
		}
		super.setCompoundDrawablesRelative(drawable, null, null, null);
	}
}
