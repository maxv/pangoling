package org.pangoling.android.app.utils.tts;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import org.pangoling.android.app.utils.Logger;

import java.util.ArrayDeque;
import java.util.Locale;
import java.util.Queue;

public class AndroidTextToSpeechHelperFragment extends TextToSpeechHelperFragment {
	private static final float DEFULT_SPEECH_RATE = 0.5f;
	private TextToSpeech mTextToSpeechEngine;
	private boolean mIsInitialized;
	private final Queue<String> mStringQueue = new ArrayDeque<>();
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		mTextToSpeechEngine = new TextToSpeech(
			context,
			this::onInitialized
		);
	}

	@Override
	public void onDetach() {
		mIsInitialized = false;
		mTextToSpeechEngine.shutdown();
		super.onDetach();
	}

	public void setLanguage(Locale loc) {
		mTextToSpeechEngine.setLanguage(loc);
	}
	
	private void onInitialized(int status) {
		Logger.event(Logger.Events.TTS_ANDROID_INITIALIZED, status);
		if (status == TextToSpeech.SUCCESS) {
			mIsInitialized = true;
			mTextToSpeechEngine.setSpeechRate(DEFULT_SPEECH_RATE);
			for(String s: mStringQueue) {
				pronounce(s);
			}
		}
	}
	
	public void speakWhenReady(String s) {
		if (!mIsInitialized) {
			mStringQueue.add(s);
		} else {
			pronounce(s);
		}
	}
	
	private void pronounce(String s) {
		mTextToSpeechEngine.speak(s, TextToSpeech.QUEUE_ADD, null, s);
	}
}
