package org.pangoling.android.app.settings.viewholders;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.pangoling.android.app.settings.Settings;

public abstract class SettingsViewHolder extends RecyclerView.ViewHolder {
	@NonNull
	protected final SharedPreferences mSharedPreferences;

	public static abstract class Factory {
		public abstract SettingsViewHolder construct(
			@NonNull ViewGroup vp, 
			@NonNull SharedPreferences sp
		);
	}
	
	public SettingsViewHolder(
		@NonNull View itemView, 
		@NonNull SharedPreferences sharedPreferences
	) {
		super(itemView);
		mSharedPreferences = sharedPreferences;
	}

	public abstract void bindSettings(Settings settings);
}
