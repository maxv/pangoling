package org.pangoling.android.app.model;

import java.util.List;

public interface TaskImage {
	enum SolveType {
		EXACTLY,
		SO_SIMILAR,
		WRONG
	}

	String getImageName();
	SolveType solve(List<String> text);
	int getSolveCount();
	int getFailCount();
	String answer();
	TaskInfo getTaskInfo();
}
