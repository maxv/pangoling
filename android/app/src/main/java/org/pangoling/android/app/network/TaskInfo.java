package org.pangoling.android.app.network;

import java.util.ArrayList;

public class TaskInfo {
	public final String answer;
	public final ArrayList<String> images;

	public TaskInfo(String answer, ArrayList<String> images) {
		this.answer = answer;
		this.images = images;
	}
}
