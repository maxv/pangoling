package org.pangoling.android.app.gui.courses;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.orm.dsl.NotNull;

import org.pangoling.android.app.R;
import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.model.ModelAccess;

import io.reactivex.disposables.Disposable;

public class CourseInfoViewHolder extends RecyclerView.ViewHolder {

	@NonNull
	private final TextView mName;

	@NotNull
	private final TextView mTasksCount;

	@NonNull
	private final SimpleDraweeView mSimpleDraweeView;

	@NonNull
	private final OnActionListener mStartActionListener;

	@NonNull
	private final OnActionListener mAboutActionListener;

	public interface OnActionListener {
		void run(Context context, int adapterPosition);
	}

	public static CourseInfoViewHolder create(
		@NonNull ViewGroup parent,
		@NonNull OnActionListener startActionListener,
		@NonNull OnActionListener aboutActionListener
	) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View rootView = inflater.inflate(
			R.layout.course_info_view_holder,
			parent,
			false
		);

		return new CourseInfoViewHolder(
			rootView,
			startActionListener,
			aboutActionListener
		);
	}

	private CourseInfoViewHolder(
		@NonNull View itemView,
		@NonNull OnActionListener startActionListener,
		@NonNull OnActionListener aboutActionListener
	) {
		super(itemView);
		mName = (TextView) itemView.findViewById(R.id.course_info_view_holder__text_name);
		mTasksCount = (TextView) itemView.findViewById(R.id.course_info_view_holder__text_tasks_count);
		mSimpleDraweeView = (SimpleDraweeView) itemView.findViewById(R.id.course_info_view_holder__image);
		mStartActionListener = startActionListener;
		mAboutActionListener = aboutActionListener;

		View start = itemView.findViewById(R.id.course_info_view_holder__button_start);
		start.setOnClickListener(this::onStartClicked);

		View about = itemView.findViewById(R.id.course_info_view_holder__button_about);
		about.setOnClickListener(this::onAboutClicked);
		
		itemView.setOnClickListener(this::onStartClicked);
	}

	private void onAboutClicked(View view) {
		mAboutActionListener.run(
			view.getContext(),
			getAdapterPosition()
		);
	}

	private void onStartClicked(View view) {
		mStartActionListener.run(
			view.getContext(),
			getAdapterPosition()
		);
	}

	@Nullable
	private Disposable mDisposable;
	
	public void set(CourseInfo courseInfo) {
		mName.setText(courseInfo.getName());
		
		if (mDisposable != null) {
			mDisposable.dispose();
		}
		
		mDisposable = ModelAccess
			.getTasksCount(courseInfo)
			.map(this::taskCountString)
			.subscribe(mTasksCount::setText);
		mTasksCount.setText("");
		
		String imagePath = courseInfo.getUrl() + courseInfo.getImageName();
		Uri uri = Uri.parse(imagePath);
		mSimpleDraweeView.setImageURI(uri);
	}
	
	private String taskCountString(Long tasksCount) {
		return itemView.getContext().getString(
			R.string.course_info_view_holder__tasks_count, 
			tasksCount
		);
	}
}
