package org.pangoling.android.app.gui.tasks.deck;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import org.pangoling.android.app.R;

public class DeckLayoutManager extends RecyclerView.LayoutManager {
	private static final int MAX_CARDS_COUNT = 5;
	@NonNull
	private final Context mContext;

	private final int m6dp;

	public DeckLayoutManager(@NonNull Context context) {
		mContext = context;
		m6dp = context.getResources().getDimensionPixelSize(R.dimen.word_card__default_margin);
	}

	public DeckLayoutManager(@NonNull Context context, AttributeSet attr, int a, int b) {
		this(context);
	}

	private final SparseArray<View> mCache = new SparseArray<>();

	private SparseArray<View> mDeletedCache = new SparseArray<>();

	@Override
	public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
		layout(recycler, state);
	}

	int[] right =  {0, 1,  1,  -1, -1};
	int[] bottom = {0, 1, -1,  1, -1};
	int[] rotations = {0, 1, -1, 1, -1};

	private void layout(RecyclerView.Recycler recycler, RecyclerView.State state) {
		mCache.clear();
		int viewsCount = getChildCount();
		for(int i = 0; i < viewsCount; ++i) {
			View v = getChildAt(i);
			int position = getPosition(v);
			mCache.append(position, v);
		}

		int cacheSize = mCache.size();
		for(int i = 0; i < cacheSize; ++i) {
			View v = mCache.valueAt(i);
			detachView(v);
		}

		int allHeight = getHeight() - getPaddingTop() - getPaddingBottom();
		int allWidth = getWidth() - getPaddingLeft() - getPaddingRight();

		int count = Math.min(state.getItemCount(), MAX_CARDS_COUNT);

		for(int position = count - 1; position >= 0; --position) {
			View view = mCache.get(position);

			if (view == null) {
				view = recycler.getViewForPosition(position);
				addView(view);
				measureChild(view, 0, 0);
			} else {
				attachView(view);
				mCache.remove(position);
			}

			int height = getDecoratedMeasuredHeight(view);
			int dT = position % bottom.length;
			int directionTop = bottom[dT];
			int top = (allHeight - height) / 2 + m6dp * directionTop * 3;

			int dL = position % right.length;
			int directionLeft = right[dL];
			int width = getDecoratedMeasuredWidth(view);
			int left = (allWidth - width) / 2 + m6dp * directionLeft * 3;

			final int rotationMult = 15;
			int rT = position % rotations.length;
			int rotation = rotations[rT];
			view.animate().rotation(rotation*rotationMult).start();

			layoutDecorated(view, left, top, left + width, top + height);
		}

		cacheSize = mCache.size();
		for(int i = 0; i < cacheSize; ++i) {
			View v = mCache.valueAt(i);
			removeAndRecycleView(v, recycler);
		}

		mDeletedCache.clear();
	}

	@Override
	public RecyclerView.LayoutParams generateDefaultLayoutParams() {
		return new RecyclerView.LayoutParams(
			ViewGroup.LayoutParams.WRAP_CONTENT,
			ViewGroup.LayoutParams.WRAP_CONTENT
		);
	}
}
