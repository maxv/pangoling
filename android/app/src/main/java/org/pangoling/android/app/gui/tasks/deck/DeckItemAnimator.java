package org.pangoling.android.app.gui.tasks.deck;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;

public class DeckItemAnimator extends RecyclerView.ItemAnimator {

	private final DefaultItemAnimator mDefaultItemAnimator = new DefaultItemAnimator();
	private final static long ANIMATION_DURATION = 250;
	{
		mDefaultItemAnimator.setAddDuration(ANIMATION_DURATION);
		mDefaultItemAnimator.setChangeDuration(ANIMATION_DURATION);
		mDefaultItemAnimator.setMoveDuration(ANIMATION_DURATION);
		mDefaultItemAnimator.setRemoveDuration(ANIMATION_DURATION);
	}

	@Override
	public boolean animateDisappearance(
		@NonNull RecyclerView.ViewHolder viewHolder,
		@NonNull ItemHolderInfo preLayoutInfo,
		@Nullable ItemHolderInfo postLayoutInfo
	) {
		return mDefaultItemAnimator.animateDisappearance(viewHolder, preLayoutInfo, postLayoutInfo);
	}

	@Override
	public boolean animateAppearance(
		@NonNull RecyclerView.ViewHolder viewHolder,
		@Nullable ItemHolderInfo preLayoutInfo,
		@NonNull ItemHolderInfo postLayoutInfo
	) {
		return mDefaultItemAnimator.animateAppearance(viewHolder, preLayoutInfo, postLayoutInfo);
	}

	@Override
	public boolean animatePersistence(
		@NonNull RecyclerView.ViewHolder viewHolder,
		@NonNull ItemHolderInfo preLayoutInfo,
		@NonNull ItemHolderInfo postLayoutInfo
	) {
		return mDefaultItemAnimator.animatePersistence(viewHolder, preLayoutInfo, postLayoutInfo);
	}

	@Override
	public boolean animateChange(
		@NonNull RecyclerView.ViewHolder oldHolder,
		@NonNull RecyclerView.ViewHolder newHolder,
		@NonNull ItemHolderInfo preLayoutInfo,
		@NonNull ItemHolderInfo postLayoutInfo
	) {
		return mDefaultItemAnimator.animateChange(
			oldHolder,
			newHolder,
			preLayoutInfo,
			postLayoutInfo
		);
	}

	@Override
	public void runPendingAnimations() {
		mDefaultItemAnimator.runPendingAnimations();
	}

	@Override
	public void endAnimation(RecyclerView.ViewHolder item) {
		mDefaultItemAnimator.endAnimation(item);
	}

	@Override
	public void endAnimations() {
		mDefaultItemAnimator.endAnimations();
	}

	@Override
	public boolean isRunning() {
		return mDefaultItemAnimator.isRunning();
	}
}
