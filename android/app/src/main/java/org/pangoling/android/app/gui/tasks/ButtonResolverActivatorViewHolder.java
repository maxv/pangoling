package org.pangoling.android.app.gui.tasks;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.pangoling.android.app.R;
import org.pangoling.android.app.gui.general.FlatButton;
import org.pangoling.android.app.resolvers.ResolverActivatorViewHolder;
import org.pangoling.android.app.resolvers.ResolverFragment;

public class ButtonResolverActivatorViewHolder extends ResolverActivatorViewHolder {
	public static class Factory extends ResolverActivatorViewHolder.Factory {
		@NonNull
		@Override
		public ResolverActivatorViewHolder construct(
			ViewGroup parent,
			@NonNull ResolverActivationListener listener
		) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			View v = inflater.inflate(R.layout.button_resolver_activator_view_holder, parent, false);
			return new ButtonResolverActivatorViewHolder((FlatButton) v, listener);
		}
	}

	@NonNull
	private final FlatButton mFlatButton;

	@NonNull
	private ResolverFragment.ResolverActivator mActivator;

	@NonNull
	private final ResolverActivationListener mListener;

	public ButtonResolverActivatorViewHolder(
		@NonNull FlatButton itemView,
		@NonNull ResolverActivationListener listener
	) {
		super(itemView);
		mFlatButton = itemView;
		itemView.setOnClickListener(mOnClicked);
		mListener = listener;
	}

	@Override
	public void bind(@NonNull ResolverFragment.ResolverActivator activator) {
		mFlatButton.setText(activator.getName());
		mFlatButton.setImageRes(activator.getIconId());
		mActivator = activator;
	}

	private View.OnClickListener mOnClicked = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			ResolverFragment resolverFragment = mActivator.createFragment();
			mListener.onResolverActivated(resolverFragment);
		}
	};
}
