package org.pangoling.android.app.resolvers;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class ResolverActivatorsAdapter extends RecyclerView.Adapter<ResolverActivatorViewHolder> {

	@NonNull
	private final List<ResolverFragment.ResolverActivator> mActivatorList = new ArrayList<>();

	@NonNull
	private final ResolverActivatorViewHolder.Factory mFactory;

	@NonNull
	private final ResolverActivatorViewHolder.ResolverActivationListener mListener;

	public ResolverActivatorsAdapter(
		@NonNull ResolverActivatorViewHolder.Factory factory,
		@NonNull ResolverActivatorViewHolder.ResolverActivationListener listener
	) {
		mFactory = factory;
		mListener = listener;
	}

	public void setActivators(List<ResolverFragment.ResolverActivator> list) {
		mActivatorList.clear();
		mActivatorList.addAll(list);
		notifyDataSetChanged();
	}

	@Override
	public ResolverActivatorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return mFactory.construct(parent, mListener);
	}

	@Override
	public void onBindViewHolder(ResolverActivatorViewHolder holder, int position) {
		ResolverFragment.ResolverActivator activator = mActivatorList.get(position);
		holder.bind(activator);
	}

	@Override
	public int getItemCount() {
		return mActivatorList.size();
	}
}
