package org.pangoling.android.app.settings;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import org.pangoling.android.app.settings.viewholders.SettingsViewHolder;

class SettingsAdapter extends RecyclerView.Adapter<SettingsViewHolder> {
	@NonNull
	private final Settings[] mSettings = Settings.values();
	
	@NonNull
	private final SharedPreferences mSharedPreferences;

	public SettingsAdapter(@NonNull SharedPreferences sharedPreferences) {
		mSharedPreferences = sharedPreferences;
	}

	@Override
	public SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return Settings.Type.values()[viewType].factory.construct(parent, mSharedPreferences);
	}

	@Override
	public void onBindViewHolder(SettingsViewHolder holder, int position) {
		holder.bindSettings(mSettings[position]);
	}

	@Override
	public int getItemViewType(int position) {
		return mSettings[position].type.ordinal();
	}

	@Override
	public int getItemCount() {
		return mSettings.length;
	}
}
