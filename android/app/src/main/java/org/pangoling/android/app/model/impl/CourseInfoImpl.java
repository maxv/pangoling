package org.pangoling.android.app.model.impl;

import com.orm.SugarRecord;

import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.model.TaskInfo;

import java.util.List;

public class CourseInfoImpl extends SugarRecord implements CourseInfo {
	private String name;
	private String description;
	private String url;
	private String imageName;
	private String uuid;

	public CourseInfoImpl() {}

	public CourseInfoImpl(String url, org.pangoling.android.app.network.CourseInfo c) {
		name = c.name;
		description = c.description;
		this.url = url;
		imageName = c.imageName;
		uuid = c.uuid;
	}
	
	private void temporary_removeCoursesWithoutUuids() {
		deleteAll(CourseInfoImpl.class, "uuid is null");
	}

	@Override
	public long save() {
		Long id = getId();
		if (id == null) {
			// actually not into db
			try {
				temporary_removeCoursesWithoutUuids();
				
				List<CourseInfoImpl> list = find(
					CourseInfoImpl.class,
					"uuid = ?",
					uuid
				);

				if (!list.isEmpty()) {
					Long newId = list.get(0).getId();
					setId(newId);
				}
			} catch (Throwable e) {
				// TODO: log errors
				// TODO: BI here
			}
		}

		return super.save();
	}

	public List<TaskInfoImpl> getAllTasksImpl() {
		return TaskInfoImpl.find(
			TaskInfoImpl.class,
			"course_info = ?",
			String.valueOf(getId())
		);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public String getImageName() {
		return imageName;
	}

	@Override
	public List<? extends TaskInfo> getTasks() {
		return getAllTasksImpl();
	}
}
