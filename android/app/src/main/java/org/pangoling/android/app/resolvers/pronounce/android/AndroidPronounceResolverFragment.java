package org.pangoling.android.app.resolvers.pronounce.android;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.pangoling.android.app.R;
import org.pangoling.android.app.resolvers.ResolverFragment;
import org.pangoling.android.app.utils.Logger;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class AndroidPronounceResolverFragment extends ResolverFragment {
	private static final String TAG = AndroidPronounceResolverFragment.class.getName();

	public static ResolverActivator createActivator() {
		return new Activator();
	}

	private View mRequestPermissionView;
	private View mStartResolvingView;

	private TextView mDescription;

	@Nullable
	private SpeechRecognizer mRecognizer;

	@Nullable
	private SpeechRecognizer mRecognizerInProgress;

	@NonNull
	private final Handler mUiHandler = new Handler();

	@Override
	public void onRequestPermissionsResult(
		int requestCode,
		@NonNull String[] permissions,
		@NonNull int[] grantResults
	) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		Logger.event(Logger.Events.REQUEST_PERMISSION_RESULT, hasPermission());
	}

	@Nullable
	@Override
	public View onCreateView(
		LayoutInflater inflater,
		@Nullable ViewGroup container,
		@Nullable Bundle savedInstanceState
	) {
		View rootView = inflater.inflate(
			R.layout.android_pronounce_resolver_fragment,
			container,
			false
		);

		mRequestPermissionView = rootView.findViewById(
			R.id.android_pronounce_resolver_fragment__permission_required
		);

		View requestPermissionButton = mRequestPermissionView.findViewById(
			R.id.android_pronounce_resolver_fragment__permission_required__request_permission_button
		);
		requestPermissionButton.setOnClickListener(
			(View v) -> requestPermission()
		);

		mStartResolvingView = rootView.findViewById(
			R.id.android_pronounce_resolver_fragment__permission_granted
		);

		View startRecognizeButton = mStartResolvingView.findViewById(
			R.id.android_pronounce_resolver_fragment__permission_granted__speach_button
		);

		mDescription = (TextView) mStartResolvingView.findViewById(
			R.id.android_pronounce_resolver_fragment__permission_granted__description
		);

		startRecognizeButton.setOnTouchListener(new TouchToRecognize());

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		updateViewByPermission();
	}

	@Override
	public void onDetach() {
		if (mRecognizer != null) {
			mRecognizer.destroy();
			mRecognizer = null;
		}
		super.onDetach();
	}

	private void updateViewByPermission() {
		if (hasPermission()) {
			mRequestPermissionView.setVisibility(View.INVISIBLE);
			mStartResolvingView.setVisibility(View.VISIBLE);
			configureRecognizer();
		} else {
			Logger.event(Logger.Events.UPDATE_VIEW_WITHOUT_AUDIO_PERMISSION);
			mRequestPermissionView.setVisibility(View.VISIBLE);
			mStartResolvingView.setVisibility(View.INVISIBLE);
			mRecognizer = null;
		}
	}

	private void configureRecognizer() {
		if (mRecognizer == null) {
			boolean recognitionAvailable = SpeechRecognizer.isRecognitionAvailable(getContext());
			if (recognitionAvailable) {
				mRecognizer = SpeechRecognizer.createSpeechRecognizer(mStartResolvingView.getContext());
				mRecognizer.setRecognitionListener(mRecognitionListener);
				mDescription.setText(R.string.pronounce_resolver__press_and_hold);
			} else {
				Logger.event(Logger.Events.GOOGLE_PRONOUNCE_RECOGNITION_NOT_AVAILABLE_IN_RECOGNITION_FRAGMENT);
			}
		}
	}

	private void requestPermission() {
		Logger.event(Logger.Events.REQUEST_AUDIO_PERMISSION);

		String[] permissions = {Manifest.permission.RECORD_AUDIO};
		requestPermissions(permissions, 0);
	}

	private boolean hasPermission() {
		boolean result = false;

		Context context = getContext();
		if (context != null) {
			int permissionCheck = ContextCompat.checkSelfPermission(
				context,
				Manifest.permission.RECORD_AUDIO
			);

			result = PackageManager.PERMISSION_GRANTED == permissionCheck;
		}

		return result;
	}

	private static final class Activator implements ResolverActivator {
		@Override
		public int getName() {
			return R.string.resolver_button_names__pronounce;
		}

		@Override
		public ResolverFragment createFragment() {
			return new AndroidPronounceResolverFragment();
		}

		@Override
		public int getIconId() {
			return R.drawable.resolver_pronounce;
		}

		@Override
		public boolean isAvailable(Context context) {
			return SpeechRecognizer.isRecognitionAvailable(context);
		}
	}

	private RecognitionListener mRecognitionListener = new RecognitionListener() {
		@Override
		public void onReadyForSpeech(Bundle params) {
			mDescription.setText(R.string.pronounce_resolver__start_speaking);
		}

		@Override
		public void onBeginningOfSpeech() {
			mDescription.setText(R.string.pronounce_resolver__listen_you);
		}

		@Override
		public void onRmsChanged(float rmsdB) {
			// not interested
		}

		@Override
		public void onBufferReceived(byte[] buffer) {
			// not interested
		}

		@Override
		public void onEndOfSpeech() {
			mRecognizerInProgress = null;
			mUiHandler.removeCallbacks(mForceCancelRecognition);
			mDescription.setText(R.string.pronounce_resolver__recognition_in_progress);
		}

		@Override
		public void onError(int error) {
			if (error == SpeechRecognizer.ERROR_NO_MATCH) {
				Logger.event(Logger.Events.AUDIO_ERROR_EMPTY);
				mDescription.setText(R.string.pronounce_resolver__something_goes_wrong);
			} else if ((error == SpeechRecognizer.ERROR_NETWORK) || (error == SpeechRecognizer.ERROR_NETWORK_TIMEOUT)){
				Logger.event(Logger.Events.AUDIO_ERROR_NETWORK);
				mDescription.setText(R.string.pronounce_resolver__network_wrong);
			}else {
				Logger.event(Logger.Events.RECOGNITION_ERROR, error);
				mDescription.setText(R.string.pronounce_resolver__something_goes_wrong);
			}
			
			mRecognizerInProgress = null;
			mUiHandler.removeCallbacks(mForceCancelRecognition);
		}

		@Override
		public void onResults(Bundle results) {
			ArrayList<String> strings = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
			if(strings != null) {
				onResult(strings);
			} else {
				Logger.event(Logger.Events.AUDIO_RESULT_EMPTY);
				mDescription.setText(R.string.pronounce_resolver__something_goes_wrong);
			}
		}

		@Override
		public void onPartialResults(Bundle partialResults) {
			// Is it a good position to show to user what we already have?
		}

		@Override
		public void onEvent(int eventType, Bundle params) {
			// never used
		}
	};

	private class TouchToRecognize implements View.OnTouchListener {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int action = event.getAction();
			if (action == MotionEvent.ACTION_UP) {
				stopRecognize();
			} else if (action == MotionEvent.ACTION_DOWN) {
				startRecognize();
			}
			return false;
		}
	}

	private void stopRecognize() {
		if (mRecognizerInProgress != null) {
			// Looks like it never works
			mRecognizerInProgress.stopListening();
			mDescription.setText(R.string.pronounce_resolver__prepare_for_recognition);
			mUiHandler.postDelayed(
				mForceCancelRecognition,
				TimeUnit.SECONDS.toMillis(20)
			);
		}
	}

	private void startRecognize() {
		if (mRecognizerInProgress != null) {
			Logger.logException(TAG, new IllegalStateException("mRecognizerInProgress != null"));
			mUiHandler.removeCallbacks(mForceCancelRecognition);
			mForceCancelRecognition.run();
		}

		if (mRecognizer != null) {
			Intent intent = constructRecognitionIntent();
			mRecognizer.startListening(intent);
			mRecognizerInProgress = mRecognizer;
			mDescription.setText(R.string.pronounce_resolver__still_hold_it);
			Logger.event(Logger.Events.RECOGNITION_STARTED);
		} else {
			Logger.event(Logger.Events.START_RECOGNIZE_WITHOUT_RECOGNIZER);
			updateViewByPermission();
		}
	}

	private final Runnable mForceCancelRecognition = new Runnable() {
		@Override
		public void run() {

			if ((mRecognizerInProgress != null) && (mRecognizerInProgress == mRecognizer)) {
				Logger.event(Logger.Events.RECOGNITION_FORCE_CANCEL);

				mRecognizerInProgress.destroy();
				mRecognizerInProgress = null;

				if (hasPermission()) {
					mDescription.setText(R.string.pronounce_resolver__something_goes_wrong);
					configureRecognizer();
				} else {
					updateViewByPermission();
				}
			}
		}
	};
	
	private static final String DEFAULT_LANGUAGE = "en-US";
	private static final int DEFAULT_MAX_RESULTS = 15;

	private static Intent constructRecognitionIntent() {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(
			RecognizerIntent.EXTRA_LANGUAGE_MODEL,
			RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
		);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, DEFAULT_LANGUAGE);

		intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, DEFAULT_MAX_RESULTS);

		// Looks like it never works
		// intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS ...

		return intent;
	}
}
