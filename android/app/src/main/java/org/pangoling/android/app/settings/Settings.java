package org.pangoling.android.app.settings;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import org.pangoling.android.app.R;
import org.pangoling.android.app.settings.viewholders.BooleanSettingsViewHolder;
import org.pangoling.android.app.settings.viewholders.SettingsViewHolder;

public enum Settings {
	SHOW_ADVERTISING(Type.BOOLEAN, R.string.settings__show_adverising),
	SHOW_TASK_STATISTIC(Type.BOOLEAN, R.string.settings__show_task_statistics);

	public enum Type {
		BOOLEAN(new BooleanSettingsViewHolder.Factory());
		
		@NonNull
		public final SettingsViewHolder.Factory factory;

		Type(@NonNull SettingsViewHolder.Factory factory) {
			this.factory = factory;
		}
	}

	@NonNull
	public final Type type;
	
	@StringRes
	public final int name;

	Settings(@NonNull Type type, @StringRes int name) {
		this.type = type;
		this.name = name;
	}

	
}
