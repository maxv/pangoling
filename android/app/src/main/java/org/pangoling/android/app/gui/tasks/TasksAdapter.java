package org.pangoling.android.app.gui.tasks;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import org.pangoling.android.app.model.CourseInfo;
import org.pangoling.android.app.model.ModelAccess;
import org.pangoling.android.app.model.TaskImage;
import org.pangoling.android.app.model.TaskListGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TasksAdapter extends RecyclerView.Adapter<TasksViewHolder> {
	private static final int TASK_REQUEST_SIZE = 20;
	private static final int MINIMAL_SIZE = 10;

	private CourseInfo mCourse;
	private final List<TaskImage> mTaskImages = new ArrayList<>();
	private boolean mShowStatistic = false;

	@Override
	public TasksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return TasksViewHolder.create(parent);
	}

	@Override
	public void onBindViewHolder(TasksViewHolder holder, int position) {
		TaskImage taskImage = getTaskImageFor(position);
		holder.bind(taskImage, mCourse.getUrl(), mShowStatistic);
	}

	@Override
	public int getItemCount() {
		return mTaskImages.size();
	}

	public void onRemoved(TasksViewHolder tasksViewHolder) {
		int position = tasksViewHolder.getAdapterPosition();
		removeByIndex(position);
	}

	private TaskImage getTaskImageFor(int position) {
		return mTaskImages.get(position);
	}

	public void removeTop() {
		removeByIndex(0);
	}

	private void removeByIndex(int index) {
		mTaskImages.remove(index);
		notifyItemRemoved(index);
		if (mTaskImages.size() < MINIMAL_SIZE) {
			requestAdditionalTasksLoad();
		}
	}

	private void requestAdditionalTasksLoad() {
		ModelAccess
			.nextTasksIssues(mCourse, TASK_REQUEST_SIZE)
			.subscribe(this::addTaskImages);
	}
	
	private void addTaskImages(List<TaskImage> images) {
		int start = mTaskImages.size();
		mTaskImages.addAll(images);
		notifyItemRangeInserted(start, images.size());
	}

	public void setCourse(CourseInfo course) {
		mCourse = course;
		int size = mTaskImages.size();
		mTaskImages.clear();
		notifyItemRangeRemoved(0, size);
		requestAdditionalTasksLoad();
	}

	public TaskImage getTop() {
		return mTaskImages.get(0);
	}

	public void setShowStatistic(boolean showStatistic) {
		if (mShowStatistic != showStatistic) {
			mShowStatistic = showStatistic;
			notifyDataSetChanged();
		}
	}
}
