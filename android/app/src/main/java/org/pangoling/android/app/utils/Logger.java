package org.pangoling.android.app.utils;


import android.app.Application;
import android.util.Log;
import com.yandex.metrica.YandexMetrica;

import org.pangoling.android.app.BuildConfig;
import org.pangoling.android.app.R;

import java.util.Map;

public class Logger {
	public static final String TAG_NON_FATAL = "NON_FATAL";
	private static final String TAG = Logger.class.getName();

	static public void init(Application application) {
		if(BuildConfig.PRODUCTION_LOGS) {
			YandexMetrica.activate(application, application.getString(R.string.yametrika_id));
			YandexMetrica.setTrackLocationEnabled(false);
			YandexMetrica.enableActivityAutoTracking(application);
		}

		event(Events.LAUNCH);
	}

	static public void logException(String tag, Throwable e) {
		logException(tag, e.getMessage(), e);
	}

	static public void logException(String tag, String message, Throwable e) {
		if(BuildConfig.PRODUCTION_LOGS) {
			YandexMetrica.reportError("" + tag + " with " + message, e);
		} else {
			Log.e(tag, "" + message, e);
		}
	}

	static public void debug(String tag, String event) {
		if(!BuildConfig.PRODUCTION_LOGS) {
			Log.e(tag, event);
		}
	}

	public static void e(String tag, String s, Throwable e) {
		logException(tag, s, e);
	}

	public static void e(String tag, String s, Object... params) {
		Throwable t = null;
		if (params.length > 0) {
			int last = params.length - 1;
			t = (params[last] instanceof Throwable) ? (Throwable)params[last] : null;
		}

		logException(tag, String.format(s, params), t);
	}

	public static void e(String tag, Throwable e) {
		logException(tag, e);
	}

	public enum Events {
		LAUNCH,

		UPDATE_VIEW_WITHOUT_AUDIO_PERMISSION,
		REQUEST_AUDIO_PERMISSION,
		REQUEST_PERMISSION_RESULT,
		AUDIO_RESULT_EMPTY,
		AUDIO_ERROR_EMPTY,
		AUDIO_ERROR_NETWORK,
		START_RECOGNIZE_WITHOUT_RECOGNIZER,
		RECOGNITION_STARTED,
		RECOGNITION_ERROR,
		RECOGNITION_FORCE_CANCEL,
		GOOGLE_PRONOUNCE_RECOGNITION_NOT_AVAILABLE,
		GOOGLE_PRONOUNCE_RECOGNITION_NOT_AVAILABLE_IN_RECOGNITION_FRAGMENT,
		ANSWER_RESULT,

		TTS_YA_WRONG_RESPONSE,

		COURSE_START,
		COURSE_ABOUT,
		
		TTS_ANDROID_INITIALIZED,
	}

	public static void event(Events e) {
		if (BuildConfig.PRODUCTION_LOGS) {
			YandexMetrica.reportEvent(e.toString());
		} else {
			Log.i(TAG, e.toString());
		}
	}

	public static void event(Events e, Object o) {
		if (BuildConfig.PRODUCTION_LOGS) {
			String message = String.format("{\"%s\": \"\"}", o);
			YandexMetrica.reportEvent(e.toString(), message);
		} else {
			String message = String.format("%s: %s", e, o);
			Log.i(TAG, message);
		}
	}

	public static void event(Events e, Map<String, Object> m) {
		if (BuildConfig.PRODUCTION_LOGS) {
			YandexMetrica.reportEvent(e.toString(), m);
		} else {
			Log.i(TAG, e.toString());
			for(Map.Entry<String, Object> p : m.entrySet()) {
				String message = String.format("\t%s:%s", p.getKey(), p.getValue());
				Log.i(TAG, message);
			}
		}
	}
}