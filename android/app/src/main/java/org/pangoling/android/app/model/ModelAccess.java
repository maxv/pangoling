package org.pangoling.android.app.model;

import com.orm.SugarRecord;

import org.pangoling.android.app.model.impl.CourseInfoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.operators.observable.ObservableError;
import io.reactivex.schedulers.Schedulers;

public class ModelAccess {
	private static List<? extends org.pangoling.android.app.model.CourseInfo> loadAllCourses() {
		return SugarRecord.listAll(CourseInfoImpl.class);
	}
	
	public static Observable<ArrayList<CourseInfo>> getCourses() {
		return Observable
			.fromCallable(ModelAccess::loadAllCourses)
			.map(ArrayList<CourseInfo>::new)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread());
	}

	private static Map<LearningState, AtomicInteger> getLearningStates(CourseInfo courseInfo) {
		Map<LearningState, AtomicInteger> states = new HashMap<>();
		for (LearningState ls : LearningState.values()) {
			states.put(ls, new AtomicInteger(0));
		}
		for (TaskInfo ti : courseInfo.getTasks()) {
			for (TaskImage taskImage : ti.getImages()) {
				LearningState ls = LearningState.getLearningStateFor(taskImage);
				states.get(ls).addAndGet(1);
			}
		}
		
		return states;
	}
	
	public static Observable<Map<LearningState, AtomicInteger>> getCourseLearningState(CourseInfo ci) {
		return Observable
			.just(ci)
			.map(ModelAccess::getLearningStates)
			.subscribeOn(Schedulers.computation())
			.observeOn(AndroidSchedulers.mainThread());
	}
	
	private static List<TaskImage> getCourseImages(CourseInfo ci) {
		List<TaskImage> result = new ArrayList<>();
		for(TaskInfo ti: ci.getTasks()) {
			result.addAll(ti.getImages());
		}
		
		return result;
	}
	
	public static Observable<List<TaskImage>> getImages(CourseInfo courseInfo) {
		return Observable
			.just(courseInfo)
			.map(ModelAccess::getCourseImages)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread());
	}
	
	public static Observable<Long> getTasksCount(CourseInfo ci) {
		return ObservableError
			.just(ci)
			.map(courseInfo -> (long) ci.getTasks().size())
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread());
	}
	
	public static Observable<List<TaskImage>> nextTasksIssues(CourseInfo ci, int requestSize) {
		return Observable
			.fromCallable(new Generator(ci, requestSize))
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread());
	}

	private static class Generator implements Callable<List<TaskImage>> {

		private final CourseInfo mCourse;
		private final int mTaskRequestSize;

		private Generator(
			CourseInfo course,  
			int taskRequestSize
		) {
			mCourse = course;
			mTaskRequestSize = taskRequestSize;
		}

		@Override
		public List<TaskImage> call() throws Exception {
			return TaskListGenerator.generate(
				mCourse,
				mTaskRequestSize
			);
		}
	}
}
