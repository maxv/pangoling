package org.pangoling.android.app.utils.tts;

import android.support.v4.app.Fragment;

import java.util.Locale;


public abstract class TextToSpeechHelperFragment extends Fragment {
	public abstract void setLanguage(Locale loc);
	public abstract void speakWhenReady(String s);
}
