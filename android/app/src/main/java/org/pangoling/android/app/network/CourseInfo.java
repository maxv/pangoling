package org.pangoling.android.app.network;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CourseInfo {
	public final String uuid;
	public final String name;
	public final String description;
	public final String imageName;
	public final ArrayList<TaskInfo> tasks;

	public CourseInfo(
		String uuid,
		String name,
		String description,
		String imageName,
		ArrayList<TaskInfo> tasks
	) {
		this.uuid = uuid;
		this.name = name;
		this.description = description;
		this.imageName = imageName;
		this.tasks = tasks;
	}

	public interface LoadListener {
		void onLoaded(
			@NonNull String url,
			@NonNull List<CourseInfo> courseInfoList
		);
	}

	public interface ErrorListener {
		void onError(
			@NonNull String url,
			@NonNull Exception exception
		);
	}

	public static void loadFromWeb(
		@NonNull String url,
		@NonNull OkHttpClient okHttpClient,
		@NonNull Gson gson,
		@NonNull LoadListener loadListener,
		@NonNull ErrorListener errorListener
	) {

		Request request = new Request.Builder()
			.url(url)
			.build();

		Callback callback = new RequestListener(
			gson,
			loadListener,
			errorListener
		);

		okHttpClient.newCall(request).enqueue(callback);
	}

	private static final class RequestListener implements Callback {
		@NonNull
		private final Gson mGson;

		@NonNull
		private final LoadListener mLoadListener;

		@NonNull
		private final ErrorListener mErrorListener;

		private RequestListener(
			@NonNull Gson gson,
			@NonNull LoadListener loadListener,
			@NonNull ErrorListener errorListener
		) {
			mGson = gson;
			mLoadListener = loadListener;
			mErrorListener = errorListener;
		}

		@Override
		public void onFailure(Call call, IOException e) {
			mErrorListener.onError(
				call.request().url().toString(),
				e
			);
		}

		@Override
		public void onResponse(Call call, Response response) throws IOException {
			Type listType = new TypeToken<ArrayList<CourseInfo>>(){}.getType();

			String jsonString = response.body().string();
			HttpUrl url = call.request().url();
			try {
				List<CourseInfo> list = mGson.fromJson(
					jsonString,
					listType
				);

				HttpUrl.Builder builder = url.newBuilder();
				builder.removePathSegment(url.pathSegments().size() - 1);

				mLoadListener.onLoaded(builder.build().toString(), list);
			} catch (JsonSyntaxException e) {
				mErrorListener.onError(url.toString(), e);
			}
		}
	}
}
