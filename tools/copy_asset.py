#!/usr/bin/python

import os
import sys
from shutil import copyfile

basePath="./info/design/assets/drawable-"
targetPath="./android/app/src/main/res/drawable-"

length=len(sys.argv)
if (length < 2):
	print "Usage " + sys.argv[0] + " base_file_name [new_file_name]"
	exit(1)

baseFileName = os.path.basename(sys.argv[1])
targetFileName = baseFileName

if (length > 2):
	targetFileName = os.path.basename(sys.argv[2])

for sizeModifier in ['mdpi', 'hdpi', 'xhdpi', 'xxhdpi', 'xxxhdpi']:
	baseFullPath = basePath + sizeModifier + "/" + baseFileName
	targetFullPath = targetPath + sizeModifier + "/" + targetFileName
	print "copyfile " + baseFullPath + " " + targetFullPath
	copyfile(baseFullPath, targetFullPath)

