#!/bin/bash

my_dir="$(dirname "$0")"
source $my_dir/common.sh

align_apk() {
	c="$build_tools_path/zipalign -v -p 4 $1 $2"
	eval $c
}

sign_apk() {
	c="$build_tools_path//apksigner sign \
		--ks $1 --key-pass env:KEY_PASSWORD \
		--ks-key-alias $2 --ks-pass env:KEY_PASSWORD \
		--out $4 \
		 $3"
	eval $c
}

main() { 
	echo "Usage $0 paht-to-key path-to-android_sdk [branch_name] [password] [silence]"
	
	if [ $# -ge 3 ];
	then
		BRANCH_NAME=$3
	else
		BRANCH_NAME=`current_branch_name`
		echo "Current brunch ($BRANCH_NAME) will be used"
	fi
	
	alias=pangoling
	key_path=$1
	build_tools_path=$2/build-tools/25.0.2
	local_path=/tmp/pangoling-tmp/$BRANCH_NAME/
	zip_file=$local_path/pack.zip
	aligned_apk=$local_path/pangoling-aligned.apk
	unsigned_apk_path=$local_path/package-unsigned.apk
	signed_apk_path=$local_path/pangoling-play-signed.apk
	
	if [ $# -ge 4 ];
	then
		KEY_PASSWORD=$4
		
	else
		echo -n Password:	
		read KEY_PASSWORD
	fi
	
	export KEY_PASSWORD=$KEY_PASSWORD
	
	align_apk="$build_tools_path/zipalign -v -p 4 $unsigned_apk_path $aligned_apk" 
	
	sign_apk="$ANDROID_HOME/build-tools/25.0.2/apksigner sign \
		--ks $key_path --key-pass env:KEY_PASSWORD\
		--ks-key-alias $alias --ks-pass env:KEY_PASSWORD
		--out $signed_apk_path $unsigned_apk_path"
	
	echo Make path... 
	mkdir -p $local_path 
	mkpath_result=$?
	if [ $mkpath_result -ne 0 ];
	then
		echo Make path FAIL
		return $mkpath_result
	else
		echo Make path finished 
	fi
	
	echo Download... 
	download $BRANCH_NAME $zip_file -nv
	download_result=$?
	if [ $download_result -ne 0 ];
	then
		echo Download FAIL
		return $download_result
	else
		echo Download finished 
	fi
	
	echo Unzipping... 
	unzip -o $zip_file -d $local_path 
	unzip_result=$?
	if [ $unzip_result -ne 0 ];
	then
		echo Uzip FAIL
		return $unzip_result
	else
		echo Unzip finished 
	fi
	
	echo Align... 
	rm $aligned_apk 
	align_apk $unsigned_apk_path $aligned_apk 
	align_result=$?
	if [ $align_result -ne 0 ];
	then
		echo Align FAIL
		return $align_result
	else
		echo Align finished 
	fi
	
	echo Signing... 
	sign_apk $key_path $alias $aligned_apk $signed_apk_path 
	sign_result=$?
	if [ $sign_result -ne 0 ];
	then
		echo Signing FAIL
		return $sign_result
	else
		echo Signing finished: $signed_apk_path
		export OUTPUT_PATH=$signed_apk_path
	fi
}

if [ $# -ge 5 ];
then
	main $@ >/dev/null
	main_result=$?
	echo -n $OUTPUT_PATH
	exit $main_result
else
	echo "Silence off"
	main $@
	main_result=$?
	echo FINISH
	exit $main_result
fi


