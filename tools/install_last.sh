#! /bin/bash

my_dir="$(dirname "$0")"
source $my_dir/common.sh


main() {
	if [ $# -ne 1 ];
	then
		echo "Usage $0 <branch_name>"
		BRANCH_NAME=`current_branch_name`
		echo "Current brunch ($BRANCH_NAME) will be used"
	else
		BRANCH_NAME=$1
	fi
	
	local_path=/tmp/pangoling-tmp/$BRANCH_NAME/
	zip_file=$local_path/pack.zip
	apk_file=pangoling-release.apk	
	
	echo Make path...
	mkdir -p $local_path
	if [ $? -ne 0 ];
	then
		echo Make path FAIL
		return $?
	else
		echo Make path finished
	fi
	
	echo Download...
	download $BRANCH_NAME $zip_file
	if [ $? -ne 0 ];
	then
		Download FAIL
		return $?
	else
		Download finished
	fi
	
	echo Unzipping...
	unzip -o $zip_file -d $local_path
	if [ $? -ne 0 ];
	then
		echo Uzip FAIL
		return $?
	else
		echo Unzip finished
	fi

	echo Installing...
	adb install -r $local_path/$apk_file
	if [ $? -ne 0 ];
	then
		echo Install FAIL
		return $?
	else
		echo Install finished
	fi
	
	return 0
}

main $@

