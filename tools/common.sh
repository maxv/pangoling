#!/bin/bash

current_branch_name() {
	echo `git rev-parse --abbrev-ref HEAD`
}

download() {
	branch_name=$1
	zip_file=$2
	silence_hack=$3
	
	server_path=https://gitlab.com/maxv/pangoling/builds/artifacts/${branch_name}/download?job=package_apk
	wget  -O $zip_file $server_path $silence_hack
	
	return $?
}
